﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Mentoring_L2.AsyncAwait
{
    /// <summary>
    /// Solution with cts creation in main where input loop
    /// </summary>
    internal class Solution
    {
        /// <summary>
        /// The Main method should not be changed at all.
        /// </summary>
        /// <param name="args"></param>
        public static void Run(string[] args)
        {
            Console.WriteLine("Mentoring program L2. Async/await.V1. Task 1");
            Console.WriteLine("Calculating the sum of integers from 0 to N.");
            Console.WriteLine("Use 'q' key to exit...");
            Console.WriteLine();

            Console.WriteLine("Enter N: ");
            var input = Console.ReadLine();

            while (input?.Trim().ToUpper() != "Q")
            {
                var cts = new CancellationTokenSource();

                if (int.TryParse(input, out var n))
                {
                    var calculateTask = CalculateSum(n, cts.Token)
                        .ContinueWith((prevTask, state) =>
                        {
                            Console.WriteLine($"Sum for {n} = {prevTask.Result}.");
                            Console.WriteLine();
                            Console.WriteLine("Enter N: ");
                        }, null, TaskContinuationOptions.OnlyOnRanToCompletion);
                }
                else
                {
                    Console.WriteLine($"Invalid integer: '{input}'. Please try again.");
                    Console.WriteLine("Enter N: ");
                }

                input = Console.ReadLine();
                cts.Cancel();
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static async Task<long> CalculateSum(int n, CancellationToken cancellationToken)
        {
            try
            {
                var task = Calculator.Calculate(n, cancellationToken);
                Console.WriteLine($"The task for {n} started... Enter N to cancel the request:");
                return await task;
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine($"Sum for {n} cancelled...");
                return await Task.FromCanceled<long>(cancellationToken);
            }
        }
    }
}