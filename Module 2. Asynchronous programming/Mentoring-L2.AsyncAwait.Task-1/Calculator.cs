﻿using System.Threading;
using System.Threading.Tasks;

namespace Mentoring_L2.AsyncAwait
{
    internal static class Calculator
    {
        public static async Task<long> Calculate(int n, CancellationToken token)
        {
            var sum = await Task.Run(async () =>
            {
                long localSum = 0;

                for (int i = 0; i < n; i++)
                {
                    // i + 1 is to allow 2147483647 (Max(Int32)) 
                    localSum += (i + 1);
                    await Task.Delay(10, token);
                }

                return localSum;
            }, token);

            return sum;
        }
    }
}