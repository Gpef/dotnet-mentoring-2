﻿using System.Diagnostics;
using System.Threading.Tasks;
using AsyncAwait.CodeReviewChallenge.Services;
using Microsoft.AspNetCore.Mvc;
using AsyncAwait.Task2.CodeReviewChallenge.Models;

namespace AsyncAwait.Task2.CodeReviewChallenge.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAssistant _assistant;

        private readonly IPrivacyDataService _privacyDataService;

        public HomeController(IAssistant assistant, IPrivacyDataService privacyDataService)
        {
            _assistant = assistant;
            _privacyDataService = privacyDataService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Privacy()
        {
            ViewBag.Message = await _privacyDataService.GetPrivacyDataAsync().ConfigureAwait(false);
            return View();
        }

        public async Task<IActionResult> Help()
        {
            ViewBag.RequestInfo = await _assistant.RequestAssistanceAsync("guest").ConfigureAwait(false);
            return View();
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
