﻿using AsyncAwait.CodeReviewChallenge.Middleware;
using Microsoft.AspNetCore.Builder;

namespace AsyncAwait.CodeReviewChallenge.Extensions
{
    public static class StatisticsMiddlewareExtensions
    {
        public static IApplicationBuilder UseStatistic(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<StatisticMiddleware>();
        }
    }
}
