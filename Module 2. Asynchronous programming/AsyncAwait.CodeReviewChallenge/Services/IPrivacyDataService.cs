﻿using System.Threading.Tasks;

namespace AsyncAwait.CodeReviewChallenge.Services
{
    public interface IPrivacyDataService
    {
        Task<string> GetPrivacyDataAsync();
    }
}
