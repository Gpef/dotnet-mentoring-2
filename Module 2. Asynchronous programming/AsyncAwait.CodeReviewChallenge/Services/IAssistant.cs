﻿using System.Threading.Tasks;

namespace AsyncAwait.CodeReviewChallenge.Services
{
    public interface IAssistant
    {
        Task<string> RequestAssistanceAsync(string requestInfo);
    }
}
