﻿using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace KeyGen
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = Generate();

            Console.WriteLine($"Key: {key}");
        }

        static string Generate()
        {
            var networkInterface = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault();
            var addressBytes = networkInterface.GetPhysicalAddress().GetAddressBytes();
            var dateBytes = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());

            var keySource = addressBytes
                .Select((byteValue, index) => byteValue ^ dateBytes[index])
                .Select(intValue => intValue <= 999 ? intValue * 10 : intValue)
                .ToArray();

            return string.Join("-", keySource);
        }
    }
}