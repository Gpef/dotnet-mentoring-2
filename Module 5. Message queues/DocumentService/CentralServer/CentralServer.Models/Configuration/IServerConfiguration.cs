﻿namespace CentralServer.Models.Configuration
{
    public interface IServerConfiguration
    {
        string TempFilesFolderPath { get; }
        string FilesFolderPath { get; }
        string TempFileExtension { get; }
    }
}