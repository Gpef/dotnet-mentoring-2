﻿namespace CentralServer.Models.Configuration
{
    public class ServerConfiguration : IServerConfiguration
    {
        public string TempFilesFolderPath { get; set; }

        public string FilesFolderPath { get; set; }

        public string TempFileExtension { get; set; }
    }
}