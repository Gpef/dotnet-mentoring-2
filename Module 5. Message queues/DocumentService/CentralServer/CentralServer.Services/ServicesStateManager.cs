﻿using Common.Extensions;
using EventBus.Events.ServiceState;
using System.Collections.Generic;

namespace CentralServer.Services
{
    internal class ServicesStateManager : IServicesStateManager
    {
        private readonly object UpdateStateLock = new object();

        private readonly Dictionary<string, State> _states = new Dictionary<string, State>();

        public Dictionary<string, State> States
        {
            get
            {
                lock (UpdateStateLock)
                {
                    return new Dictionary<string, State>(_states);
                }
            }
        }

        public void Update(string serviceName, State state)
        {
            lock (UpdateStateLock)
            {
                _states.AddOrUpdate(serviceName, state);
            }
        }

        public void SetPollingState()
        {
            lock (UpdateStateLock)
            {
                foreach (var serviceName in States.Keys)
                {
                    _states.AddOrUpdate(serviceName, State.Polling);
                }
            }
        }
    }
}