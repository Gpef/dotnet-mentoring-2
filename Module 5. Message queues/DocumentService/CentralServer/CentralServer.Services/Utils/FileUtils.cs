﻿using System.IO;
using System.Threading.Tasks;

namespace CentralServer.Services.Utils
{
    public static class FileUtils
    {
        public static async Task OverriteCopyAsync(byte[] fileBytes, string destinationPath)
        {
            CreateParentDirectoryIfNeeded(destinationPath);

            using (var fileStream = File.OpenWrite(destinationPath))
            {
                await fileStream.WriteAsync(fileBytes, 0, fileBytes.Length);
            }
        }

        public static async Task ComulativeCopyAsync(string sourcePath, FileStream destStream)
        {
            using (FileStream sourceStream = File.OpenRead(sourcePath))
            {
                var buffer = new byte[1024];
                int bytesRead;

                while ((bytesRead = sourceStream.Read(buffer, 0, 1024)) > 0)
                {
                    await destStream.WriteAsync(buffer, 0, bytesRead);
                }
            }
        }

        public static void CreateParentDirectoryIfNeeded(string path)
        {
            var parentFolder = Directory.GetParent(path);

            if (!parentFolder.Exists)
            {
                Directory.CreateDirectory(parentFolder.FullName);
            }
        }
    }
}