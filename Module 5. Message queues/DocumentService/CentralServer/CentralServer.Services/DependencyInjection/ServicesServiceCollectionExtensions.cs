﻿using CentralServer.Services.Events.EventHandlers;
using CentralServer.Services.Events.EventHandlers.FileUpload;
using Microsoft.Extensions.DependencyInjection;

namespace CentralServer.Services.DependencyInjection
{
    public static class ServicesServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<FileUploadEventSequencePartHandler>();
            services.AddTransient<ServiceStateUpdateEventHandler>();

            services.AddSingleton<IServicesStateManager, ServicesStateManager>();

            return services;
        }
    }
}