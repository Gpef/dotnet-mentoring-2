﻿using System.Threading.Tasks;
using EventBus.Abstractions;
using EventBus.Events.ServiceState;

namespace CentralServer.Services.Events.EventHandlers
{
    public class ServiceStateUpdateEventHandler : IEventHandler<ServiceStateUpdateEvent>
    {
        private readonly IServicesStateManager _servicesStateManager;

        public ServiceStateUpdateEventHandler(IServicesStateManager servicesStateManager)
        {
            _servicesStateManager = servicesStateManager;
        }

        public Task Handle(ServiceStateUpdateEvent @event)
        {
            if (@event == null) { throw new ArgumentNullException(nameof(@event)); }

            _servicesStateManager.Update(@event.ServiceName, @event.ServiceState);

            return Task.CompletedTask;
        }
    }
}