﻿using EventBus.Events.ServiceState;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Services.Services.ServiceState
{
    internal class ServiceStateAccessor : IServiceStateAccessor
    {
        private readonly List<Guid> _processings = new List<Guid>();

        public State CurrentState => _processings.Any() ? State.Processing : State.Idling;

        public void AddProcessing(Guid processingId)
        {
            _processings.Add(processingId);
        }

        public void RemoveProcessing(Guid processingId)
        {
            _processings.RemoveAll(p => p == processingId);
        }
    }
}