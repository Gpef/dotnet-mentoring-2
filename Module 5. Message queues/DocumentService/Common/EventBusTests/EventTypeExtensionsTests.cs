﻿using EventBus.Events;
using EventBus.Events.EventSequence;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using EventBus.Extensions;
using EventBus.Events.Abstractions;

namespace EventBusTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public sealed class EventTypeExtensionsTests
    {
        [Test]
        [TestCaseSource(nameof(EventTypesWithNames))]
        public void GetEventNameFromType(Type eventType, string expectedName)
        {
            var actualName = eventType.GetEventNameFromType();
            Assert.AreEqual(expectedName, actualName);
        }

        private static IEnumerable<TestCaseData> EventTypesWithNames
        {
            get
            {
                yield return new TestCaseData(typeof(Event), "Event").SetName("{m}(Event)");
                yield return new TestCaseData(typeof(EventSequencePart<FileUploadEvent>), "FileUploadEventSequencePart").SetName("{m}(FileUploadEventSequencePart)");
            }
        }
    }
}