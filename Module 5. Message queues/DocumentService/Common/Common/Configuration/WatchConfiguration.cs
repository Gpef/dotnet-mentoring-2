﻿using System.Collections.Generic;

namespace Common.Configuration
{
    public class WatchConfiguration : IReloadableWatchConfiguration
    {
        public List<string> PathsToListen { get; set; }

        public List<string> Rules { get; set; }

        public void SetPathsToListen(List<string> paths)
        {
            PathsToListen = paths;
        }

        public void SetRules(List<string> rules)
        {
            Rules = rules;
        }
    }
}