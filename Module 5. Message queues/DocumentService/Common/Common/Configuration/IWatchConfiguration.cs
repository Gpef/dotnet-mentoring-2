﻿using System.Collections.Generic;

namespace Common.Configuration
{
    public interface IWatchConfiguration
    {
        List<string> PathsToListen { get; }
        List<string> Rules { get; }
    }
}