﻿using System.Collections.Generic;

namespace Common.Configuration
{
    public interface IReloadableWatchConfiguration : IWatchConfiguration
    {
        void SetPathsToListen(List<string> paths);
        void SetRules(List<string> rules);
    }
}