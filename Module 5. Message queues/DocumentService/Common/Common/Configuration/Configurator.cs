﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Common.Configuration
{
    public class Configurator
    {
        public const string EnvironmentVariableName = "ASPNETCORE_ENVIRONMENT";

        public static IConfigurationRoot Build(string configFilesPath = null, string environmentName = null)
        {
            var path = configFilesPath ?? Directory.GetCurrentDirectory();
            var env = environmentName ?? Environment.GetEnvironmentVariable(EnvironmentVariableName);
            var fileName = env?.Length > 0 ? $"appsettings.{env}.json" : "appsettings.json";
            var builder = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(fileName, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}