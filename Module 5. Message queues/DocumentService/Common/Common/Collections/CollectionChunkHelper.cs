﻿namespace Common.Collections
{
    public static class CollectionChunkHelper
    {
        public static ChunkInfo GetChunckInfo(long collectionLength, long chunkLength)
        {
            var chunksCount = (int)(collectionLength / (double)chunkLength);
            var lastChunkLength = (int)(collectionLength - chunksCount * chunkLength);
            return new ChunkInfo(chunksCount, lastChunkLength);
        }
    }
}