﻿using System.Collections.Generic;

namespace EventBus.AzureServiceBus
{
    public interface IServiceBusConfiguration
    {
        string ConnectionString { get; }
        string ClientName { get; }

        List<string> Queues { get; }
        List<string> Topics { get; }
    }
}