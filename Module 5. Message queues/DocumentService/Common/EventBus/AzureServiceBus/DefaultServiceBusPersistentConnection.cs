﻿using Common.Extensions;
using EventBus.Events.EventBusEntity;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using System;
using System.Collections.Generic;

namespace EventBus.AzureServiceBus
{
    internal class DefaultServiceBusPersistentConnection : IServiceBusPersistentConnection
    {
        private readonly IServiceBusConfiguration _serviceBusConfiguration;

        private readonly Dictionary<string, ITopicClient> _topicClients = new Dictionary<string, ITopicClient>();
        private readonly Dictionary<string, IQueueClient> _queueClients = new Dictionary<string, IQueueClient>();
        private readonly Dictionary<string, ISubscriptionClient> _subscriptionClients = new Dictionary<string, ISubscriptionClient>();

        private readonly object CreateTopicClientLock = new object();
        private readonly object CreateQueueClientLock = new object();
        private readonly object CreateSubscriptionClientLock = new object();

        private bool _disposed;

        public ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }

        public DefaultServiceBusPersistentConnection(
            ServiceBusConnectionStringBuilder serviceBusConnectionStringBuilder,
            IServiceBusConfiguration serviceBusConfiguration)
        {
            ServiceBusConnectionStringBuilder = serviceBusConnectionStringBuilder;
            _serviceBusConfiguration = serviceBusConfiguration;
        }

        public ISubscriptionClient CreateSubscriptionClient(string topicName)
        {
            _subscriptionClients.TryGetValue(topicName, out var client);

            if (IsConnectionOpened(client))
            {
                lock (CreateSubscriptionClientLock)
                {
                    if (IsConnectionOpened(client))
                    {
                        client = CreateSubscriptionClientPrivate(_serviceBusConfiguration.ClientName, topicName);

                        _subscriptionClients.AddOrUpdate(topicName, client);
                    }
                }
            }

            return client;
        }

        private static bool IsConnectionOpened(IClientEntity client) => client == null || client.IsClosedOrClosing;


        public IQueueClient CreateQueueClient(string queueName)
        {
            _queueClients.TryGetValue(queueName, out var client);

            if (IsConnectionOpened(client))
            {
                lock (CreateQueueClientLock)
                {
                    if (IsConnectionOpened(client))
                    {
                        client = CreateQueueClientPrivate(queueName);

                        _queueClients.AddOrUpdate(queueName, client);
                    }
                }
            }

            return client;
        }

        public ITopicClient CreateTopicClient(string topicName)
        {
           _topicClients.TryGetValue(topicName, out var client);

            if (IsConnectionOpened(client))
            {
                lock (CreateTopicClientLock)
                {
                    if (IsConnectionOpened(client))
                    {
                        client = CreateTopicClientPrivate(topicName);

                        _topicClients.AddOrUpdate(topicName, client);
                    }
                }
            }

            return client;
        }

        public ISenderClient CreateSenderClient(EventBusEntityInfo entity)
        {
            var name = entity.Name;
            var type = entity.Type;

            switch (type)
            {
                case EventBusEntityType.Queue:
                    return CreateQueueClient(name);
                case EventBusEntityType.Topic:
                    return CreateTopicClient(name);
                default:
                    throw new NotSupportedException($"Entity {type} is not supported.");
            }
        }

        public void Dispose()
        {
            if (_disposed) return;

            var clients = new List<IClientEntity>();
            clients.AddRange(_subscriptionClients.Values);
            clients.AddRange(_queueClients.Values);
            clients.AddRange(_topicClients.Values);

            foreach (var client in clients)
            {
                client.CloseAsync().GetAwaiter().GetResult();
            }

            _disposed = true;
        }

        private ITopicClient CreateTopicClientPrivate(string topic) => new TopicClient(ServiceBusConnectionStringBuilder.GetNamespaceConnectionString(), topic, RetryPolicy.Default);

        private ISubscriptionClient CreateSubscriptionClientPrivate(string topic, string subscription) => new SubscriptionClient(ServiceBusConnectionStringBuilder.GetNamespaceConnectionString(), topic, subscription);

        private IQueueClient CreateQueueClientPrivate(string queue) => new QueueClient(ServiceBusConnectionStringBuilder.GetNamespaceConnectionString(), queue);
    }
}