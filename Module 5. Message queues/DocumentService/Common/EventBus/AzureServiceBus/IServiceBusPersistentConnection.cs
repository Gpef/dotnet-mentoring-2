﻿using EventBus.Events.EventBusEntity;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using System;

namespace EventBus.AzureServiceBus
{
    public interface IServiceBusPersistentConnection : IDisposable
    {
        ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }

        ISenderClient CreateSenderClient(EventBusEntityInfo entity);

        ISubscriptionClient CreateSubscriptionClient(string topicName);

        IQueueClient CreateQueueClient(string queueName);

        ITopicClient CreateTopicClient(string topicName);
    }
}