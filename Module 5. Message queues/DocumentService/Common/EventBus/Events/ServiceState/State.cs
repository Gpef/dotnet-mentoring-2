﻿namespace EventBus.Events.ServiceState
{
    public enum State
    {
        None,
        Idling,
        Polling,
        Processing,
        NotResponding
    }
}