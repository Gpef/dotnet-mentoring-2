﻿using EventBus.Events.Abstractions;
using EventBus.Events.EventBusEntity;

namespace EventBus.Events.ServiceState
{
    [EventBusEntity(EventBusEntityType.Topic, "service-topic")]
    public class ServiceStateUpdateEvent : Event
    {
        public string ServiceName { get; }
        public State ServiceState { get; }

        public ServiceStateUpdateEvent(string serviceName, State serviceState)
        {
            ServiceName = serviceName;
            ServiceState = serviceState;
        }
    }
}