﻿namespace EventBus.Events.Abstractions
{
    public abstract class EventWithPayload<T> : Event
    {
        public T Payload { get; }

        public EventWithPayload(T payload)
        {
            Payload = payload;
        }
    }
}