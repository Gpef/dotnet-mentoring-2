﻿namespace EventBus.Events.EventBusEntity
{
    public class EventBusEntityInfo
    {
        public EventBusEntityType Type { get; }
        public string Name { get; }

        public EventBusEntityInfo(EventBusEntityType type, string name)
        {
            Type = type;
            Name = name;
        }
    }
}