﻿namespace EventBus.Events.EventBusEntity
{
    public enum EventBusEntityType
    {
        Queue,
        Topic
    }
}