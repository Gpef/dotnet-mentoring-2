﻿using EventBus.Events.Abstractions;
using System;

namespace EventBus.Events.EventSequence
{
    public class EventSequencePart<TEvent> : EventWithPayload<TEvent> where TEvent : Event
    {
        public Guid SequenceId { get; }
        public int Index { get; }
        public int Length { get; }

        public EventSequencePart(Guid sequenceId, int index, int length, TEvent payload) : base(payload)
        {
            SequenceId = sequenceId;
            Index = index;
            Length = length;
        }
    }
}