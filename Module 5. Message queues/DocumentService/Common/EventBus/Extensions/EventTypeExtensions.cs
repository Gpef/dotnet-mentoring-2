﻿using EventBus.Events.Abstractions;
using System;
using System.Linq;

namespace EventBus.Extensions
{
    public static class EventTypeExtensions
    {
        private const string EVENT_SUFFIX = nameof(Event);

        public static string GetEventNameFromType(this Type eventType)
        {
            if (eventType.Name == EVENT_SUFFIX)
            {
                return eventType.Name;
            }

            if (eventType.GenericTypeArguments.Any())
            {
                var argumentTypeName = eventType.GenericTypeArguments.First().Name.Replace(EVENT_SUFFIX, string.Empty);
                var genericTypeName = eventType.Name.Replace("`1", string.Empty);
                var eventName = $"{argumentTypeName}{genericTypeName}";

                if (eventName.Length > 50)
                {
                    eventName = eventName.Substring(0, 50);
                }

                return eventName;
            }

            return eventType.Name.Replace(EVENT_SUFFIX, string.Empty); ;
        }
    }
}