﻿namespace EventBus.Configuration
{
    public interface IEventSequenceFactoryConfiguration
    {
        int PartSize { get; }
    }
}