﻿namespace EventBus.Configuration
{
    public interface IReloadableEventSequenceFactoryConfiguration : IEventSequenceFactoryConfiguration
    {
        void SetPartSize(int partSize);
    }
}