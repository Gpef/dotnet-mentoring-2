﻿using Common.Collections;
using Common.Services.Services.ServiceState;
using EventBus.Configuration;
using Moq;
using NUnit.Framework;
using System.IO;
using System.Linq;
using UploadService.Services.Configuration;
using UploadService.Services.Events;

namespace UploadService.Tests.Events
{
    [TestFixture]
    public class FileUploadEventSequenceFactoryTests
    {
        private const int PartSize = 512;
        private static readonly IEventSequenceFactoryConfiguration _config = new EventSequenceFactoryConfiguration() { PartSize = PartSize };

        private static readonly Mock<IServiceStateAccessor> _serviceStateAccessor = new Mock<IServiceStateAccessor>();
        private static readonly Mock<IConfigurationManager> _configurationManager = new Mock<IConfigurationManager>();

        [SetUp]
        public void ResetMock()
        {
            _serviceStateAccessor.Reset();
            _configurationManager.Reset();
        }

        private static IFileUploadEventSequenceFactory CreateSubject()
        {
            return new FileUploadEventSequenceFactory(_configurationManager.Object, _serviceStateAccessor.Object);
        }

        private static FileStream GetTestFileStream(string fileName)
        {
            return File.OpenRead(Path.Combine(Directory.GetCurrentDirectory(), "TestData", fileName));
        }

        [Test]
        public void FileUploadEventSequenceFactory_Create_VerifyParts()
        {
            _configurationManager.SetupGet(cm => cm.EventSequenceFactoryConfiguration).Returns(_config);

            var fileName = "6607Bytes.txt";
            var factory = CreateSubject();

            using (var fileStream = GetTestFileStream(fileName))
            {
                var fileSize = fileStream.Length;
                var chunkInfo = CollectionChunkHelper.GetChunckInfo(fileSize, PartSize);
                var expectedPartsCount = chunkInfo.FullChunksCount + (chunkInfo.LastChunkLength == 0 ? 0 : 1);

                var eventSequence = factory.CreateSequence(fileStream).ToList();
                var sequenceId = eventSequence.First().SequenceId;

                Assert.Multiple(() =>
                {
                    Assert.AreEqual(expectedPartsCount, eventSequence.Count());

                    int index;
                    for (index = 0; index < chunkInfo.FullChunksCount; index++)
                    {
                        var part = eventSequence[index];
                        var @event = part.Payload;

                        Assert.AreEqual(fileName, @event.FileName);
                        Assert.AreEqual(PartSize, @event.FileBytes.Length);
                        Assert.AreEqual(index, part.Index);
                        Assert.AreEqual(expectedPartsCount, part.Length);
                    }

                    index++;

                    var lastPart = eventSequence.Last();
                    var lastEvent = lastPart.Payload;
                    Assert.AreEqual(fileName, lastEvent.FileName);
                    Assert.AreEqual(chunkInfo.LastChunkLength, lastEvent.FileBytes.Length);
                    Assert.AreEqual(index, lastPart.Index);
                    Assert.AreEqual(expectedPartsCount, lastPart.Length);
                });
            }
        }
    }
}
