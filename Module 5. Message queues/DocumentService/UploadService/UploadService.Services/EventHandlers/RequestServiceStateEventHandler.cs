﻿using System.Threading.Tasks;
using Common.Services.Services.ServiceState;
using EventBus.Abstractions;
using EventBus.Events.ServiceState;

namespace UploadService.Services.EventHandlers
{
    public class RequestServiceStateEventHandler : IEventHandler<RequestServiceStateEvent>
    {
        private readonly IServiceStateNotificationService _serviceStateNotificationService;

        public RequestServiceStateEventHandler(IServiceStateNotificationService serviceStateNotificationService)
        {
            _serviceStateNotificationService = serviceStateNotificationService;
        }

        public async Task Handle(RequestServiceStateEvent @event)
        {
            if (@event == null) { throw new ArgumentNullException(nameof(@event)); }

            await _serviceStateNotificationService.NotifyAsync();
        }
    }
}
