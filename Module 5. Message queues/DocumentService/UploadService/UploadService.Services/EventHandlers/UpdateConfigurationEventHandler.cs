﻿using System;
using System.Threading.Tasks;
using EventBus.Abstractions;
using EventBus.Events;
using UploadService.Services.Configuration;

namespace UploadService.Services.EventHandlers
{
    public class UpdateConfigurationEventHandler<TConfiguration> : IEventHandler<UpdateConfigEvent<TConfiguration>>
    {
        private readonly IConfigurationManager _configurationManager;

        public UpdateConfigurationEventHandler(IConfigurationManager configurationManager)
        {
            _configurationManager = configurationManager;
        }

        public Task Handle(UpdateConfigEvent<TConfiguration> @event)
        {
            if (@event == null) { throw new ArgumentNullException(nameof(@event)); }

            _configurationManager.Reload(@event.Payload);

            return Task.CompletedTask;
        }
    }
}