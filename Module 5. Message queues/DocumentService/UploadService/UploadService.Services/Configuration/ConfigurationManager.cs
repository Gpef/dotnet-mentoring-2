﻿using Common.Configuration;
using EventBus.Configuration;
using System;

namespace UploadService.Services.Configuration
{
    internal class ConfigurationManager : IConfigurationManager
    {
        private readonly IReloadableWatchConfiguration _watchConfiguration;
        private readonly IReloadableEventSequenceFactoryConfiguration _eventSequenceFactoryConfiguration;

        public ConfigurationManager(IReloadableWatchConfiguration watchConfiguration, IReloadableEventSequenceFactoryConfiguration eventSequenceFactoryConfiguration)
        {
            _watchConfiguration = watchConfiguration;
            _eventSequenceFactoryConfiguration = eventSequenceFactoryConfiguration;
        }

        public IWatchConfiguration WatchConfiguration => _watchConfiguration;

        public IEventSequenceFactoryConfiguration EventSequenceFactoryConfiguration => _eventSequenceFactoryConfiguration;

        public void Reload<T>(T newConfiguration)
        {
            if (newConfiguration == null) { throw new ArgumentNullException(nameof(newConfiguration)); }

            var configType = typeof(T);

            if (configType == typeof(WatchConfiguration))
            {
                Reload(newConfiguration as WatchConfiguration);
            }
            else if (configType == typeof(EventSequenceFactoryConfiguration))
            {
                Reload(newConfiguration as EventSequenceFactoryConfiguration);
            }
            else
            {
                throw new ArgumentException($"Configuration {typeof(T).Name} wasn't found in configuration manager.", nameof(newConfiguration));
            }
        }

        private void Reload(WatchConfiguration newValue)
        {
            if (newValue == null) { throw new ArgumentNullException(nameof(newValue)); }

            _watchConfiguration.SetPathsToListen(newValue.PathsToListen);
            _watchConfiguration.SetRules(newValue.Rules);
        }

        private void Reload(EventSequenceFactoryConfiguration newValue)
        {
            if (newValue == null) { throw new ArgumentNullException(nameof(newValue)); }

            _eventSequenceFactoryConfiguration.SetPartSize(newValue.PartSize);
        }
    }
}