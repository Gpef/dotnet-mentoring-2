﻿using Common.Configuration;
using EventBus.Configuration;

namespace UploadService.Services.Configuration
{
    public interface IConfigurationManager
    {
        IWatchConfiguration WatchConfiguration { get; }
        IEventSequenceFactoryConfiguration EventSequenceFactoryConfiguration { get; }

        void Reload<T>(T newConfiguration);
    }
}