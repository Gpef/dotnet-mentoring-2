﻿using Common.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UploadService.Services.Configuration;
using UploadService.Services.EventHandlers;
using UploadService.Services.Events;

namespace UploadService.Services.DependencyInjection
{
    public static class ServicesServiceCollectionExtensions
    {
        public static IServiceCollection AddUploadServiceServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddEventHandlers();

            services.AddReloadableWatchConfiguration(configuration);

            services.AddScoped<IFileSystemWatcher, FileSystemWatcher>();
            services.AddSingleton<IConfigurationManager, ConfigurationManager>();

            services.AddSingleton<IFileUploadEventSequenceFactory, FileUploadEventSequenceFactory>();

            return services;
        }

        private static IServiceCollection AddEventHandlers(this IServiceCollection services)
        {
            services.AddScoped<RequestServiceStateEventHandler>();
            services.AddScoped(typeof(UpdateConfigurationEventHandler<>));

            return services;
        }
    }
}