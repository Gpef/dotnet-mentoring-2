﻿using System;
using System.Collections.Generic;
using System.IO;
using Common.Collections;
using Common.Services.Services.ServiceState;
using EventBus.Configuration;
using EventBus.Events;
using EventBus.Events.EventSequence;
using UploadService.Services.Configuration;

namespace UploadService.Services.Events
{
    internal class FileUploadEventSequenceFactory : IFileUploadEventSequenceFactory
    {
        private const int NoOffest = 0;

        private readonly IConfigurationManager _configurationManager;
        private readonly IServiceStateAccessor _serviceStateAccessor;

        private IEventSequenceFactoryConfiguration Config => _configurationManager.EventSequenceFactoryConfiguration;

        public FileUploadEventSequenceFactory(IConfigurationManager configurationManager, IServiceStateAccessor serviceStateAccessor)
        {
            _serviceStateAccessor = serviceStateAccessor;
            _configurationManager = configurationManager;

            if (Config.PartSize < 1)
            {
                throw new ArgumentException(nameof(configurationManager), $"PartSize cannot be lower than 1. Actual: {Config.PartSize}.");
            }
        }

        public IEnumerable<EventSequencePart<FileUploadEvent>> CreateSequence(FileStream fileStream)
        {
            if (fileStream == null) { throw new ArgumentNullException(nameof(fileStream)); }

            var partSize = Config.PartSize;

            var sequenceId = Guid.NewGuid();
            _serviceStateAccessor.AddProcessing(sequenceId);

            var fileName = Path.GetFileName(fileStream.Name);
            var fileSize = fileStream.Length;

            var chunkInfo = CollectionChunkHelper.GetChunckInfo(fileSize, partSize);
            var sequenceLength = chunkInfo.FullChunksCount + (chunkInfo.LastChunkLength == 0 ? 0 : 1);

            int indexInFile, indexInSequence;
            var ceilBorder = chunkInfo.FullChunksCount * partSize;

            for (indexInFile = 0, indexInSequence = 0;
                indexInFile < ceilBorder;
                indexInFile += partSize, indexInSequence++)
            {
                var nextBlock = new byte[partSize];
                fileStream.Read(nextBlock, NoOffest, partSize);

                yield return new EventSequencePart<FileUploadEvent>(
                    sequenceId,
                    indexInSequence,
                    sequenceLength,
                    new FileUploadEvent(fileName, nextBlock));
            }

            indexInSequence++;

            var lastBlockLength = chunkInfo.LastChunkLength;
            if (lastBlockLength != 0)
            {
                var nextBlock = new byte[lastBlockLength];
                fileStream.Read(nextBlock, NoOffest, lastBlockLength);

                yield return new EventSequencePart<FileUploadEvent>(
                    sequenceId,
                    indexInSequence,
                    sequenceLength,
                    new FileUploadEvent(fileName, nextBlock));
            }

            _serviceStateAccessor.RemoveProcessing(sequenceId);
        }
    }
}