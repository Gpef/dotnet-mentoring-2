﻿using EventBus.Events;
using EventBus.Events.EventSequence;
using System.Collections.Generic;
using System.IO;

namespace UploadService.Services.Events
{
    public interface IFileUploadEventSequenceFactory
    {
        IEnumerable<EventSequencePart<FileUploadEvent>> CreateSequence(FileStream fileStream);
    }
}