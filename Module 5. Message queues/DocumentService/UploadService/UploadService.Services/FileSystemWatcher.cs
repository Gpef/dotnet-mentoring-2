﻿using Common.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UploadService.Services
{
    internal class FileSystemWatcher : IFileSystemWatcher
    {
        public event FileFound FileFoundEvent;

        private readonly object FileFoundEventLock = new object();

        event FileFound IFileSystemWatcher.FileFoundEvent
        {
            add
            {
                lock (FileFoundEventLock)
                {
                    FileFoundEvent += value;
                }
            }
            remove
            {
                lock (FileFoundEventLock)
                {
                    FileFoundEvent -= value;
                }
            }
        }

        public List<Regex> Rules { get; } = new List<Regex>();
        public List<string> WatchingDirectories { get; } = new List<string>();

        private readonly IFileSystem _fileSystem;
        private readonly ILogger<FileSystemWatcher> _logger;

        private readonly Dictionary<string, FileSystemWatcherBase> _watchers = new Dictionary<string, FileSystemWatcherBase>();

        public FileSystemWatcher(IFileSystem fileSystem, IWatchConfiguration config, ILogger<FileSystemWatcher> logger)
        {
            if (null == config) throw new ArgumentNullException(nameof(config));

            _fileSystem = fileSystem;
            _logger = logger;

            ApplyNewConfig(config);
        }

        public void ApplyNewConfig(IWatchConfiguration config)
        {
            ClearConfig();

            foreach (var currentPath in config?.PathsToListen)
            {
                AddWatchingDirectory(currentPath);
            }

            foreach (var currentRuleConfiguration in config?.Rules)
            {
                AddRule(currentRuleConfiguration);
            }
        }

        private void ClearConfig()
        {
            foreach (var watchingDirectory in WatchingDirectories)
            {
                RemoveWatchingDirectory(watchingDirectory);
            }

            foreach (var rule in Rules)
            {
                RemoveRule(rule);
            }
        }

        private void AddRule(string rule)
        {
            if (rule == null) { throw new ArgumentNullException(nameof(rule)) };

            Rules.Add(new Regex(rule));
            _logger.LogInformation($"Rule {rule} added.");
        }


        private void RemoveRule(Regex rule)
        {
            if (rule == null) { throw new ArgumentNullException(nameof(rule)) };

            var result = Rules.Remove(rule);
            _logger.LogInformation($"Rule {rule} remove result: {result}.");
        }

        private void AddWatchingDirectory(string path)
        {
            if (!_fileSystem.Directory.Exists(path))
            {
                _logger.LogWarning($"Path {path} is not exists. New directory created.");
                _fileSystem.Directory.CreateDirectory(path);
            }

            if (_watchers.ContainsKey(path))
            {
                _logger.LogInformation($"Path {path} already in tht listen list.");
            }
            else
            {
                var watcher = _fileSystem.FileSystemWatcher.FromPath(path);
                watcher.IncludeSubdirectories = false;
                watcher.NotifyFilter = NotifyFilters.FileName;
                watcher.EnableRaisingEvents = true;
                watcher.Created += HandleCreated;

                _watchers.Add(path, watcher);
                _logger.LogInformation($"Watcher for {path} added.");
            }
        }

        private void RemoveWatchingDirectory(string path)
        {
            if (path == null) { throw new ArgumentNullException(nameof(path)) };
            if (!_watchers.ContainsKey(path)) return;

            var fileSystemWatcher = _watchers[path];
            _watchers.Remove(path);
            fileSystemWatcher.Dispose();
        }

        private void HandleCreated(object sender, FileSystemEventArgs args)
        {
            if (sender == null) { throw new ArgumentNullException(nameof(sender)) };
            if (args == null) { throw new ArgumentNullException(nameof(args)) };

            if (new DirectoryWrapper(_fileSystem).Exists(args.FullPath))
            {
                return;
            }

            _logger.LogInformation("File created in watching directory: {Name}", args.Name);
            if (!_fileSystem.File.Exists(args.FullPath))
            {
                _logger.LogError("File doesn't exist anymore {FullPath}", args.FullPath);
                return;
            }

            var rule = FirstMatchingRule(args.Name);
            if (null == rule)
            {
                _logger.LogInformation("No rules found for file {Name}", args.Name);
                return;
            }

            _logger.LogInformation("Rule found for file {Name}: {Rule}", args.Name, rule);

            try
            {
                Task.Run(() => FileFoundEvent?.Invoke(args.FullPath)).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error during rising {FileFoundEvent}", nameof(FileFoundEvent));
            }
        }

        private Regex FirstMatchingRule(string fileName) =>
            Rules.Where(currentRule => currentRule.IsMatch(fileName))
                .Select(currentRule => currentRule)
                .FirstOrDefault();
    }
}