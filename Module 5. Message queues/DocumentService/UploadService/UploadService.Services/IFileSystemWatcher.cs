﻿using Common.Configuration;

namespace UploadService.Services
{
    public delegate void FileFound(string fullPath);

    public interface IFileSystemWatcher
    {
        event FileFound FileFoundEvent;

        void ApplyNewConfig(IWatchConfiguration config);
    }
}