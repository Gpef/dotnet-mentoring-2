﻿using Common.Configuration;
using Common.DependencyInjection;
using Common.Services.DependencyInjection;
using EventBus.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO.Abstractions;
using System.Threading.Tasks;
using UploadService.Service.HostedServices;
using UploadService.Services.DependencyInjection;

namespace UploadService
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var config = Configurator.Build();

            await BuildHost(config).RunAsync();
        }

        public static IHost BuildHost(IConfiguration configuration)
        {
            var host = new HostBuilder()
               .ConfigureServices((hostContext, services) =>
               {
                   services.ConfigureServices(configuration);
               })
               .ConfigureLogging((hostContext, configLogging) =>
               {
                   configLogging.AddConsole();
               })
               .UseConsoleLifetime()
               .Build();

            return host;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging();

            services.AddCommonServices();
            services.AddAppConfiguration(configuration);
            services.AddReloadableWatchConfiguration(configuration);
            services.AddAppConfiguration(configuration);

            services.AddEventBus(configuration);
            services.AddUploadServiceServices(configuration);

            services.AddHostedService<FileWatcherHostedService>();
            services.AddTransient<IFileSystem>(ctx => new FileSystem());

            return services;
        }
    }
}
