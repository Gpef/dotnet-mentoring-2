﻿using Common.Configuration;
using Common.Services.Services.ServiceState;
using EventBus.Abstractions;
using EventBus.Configuration;
using EventBus.Events;
using EventBus.Events.ServiceState;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UploadService.Services;
using UploadService.Services.EventHandlers;
using UploadService.Services.Events;

namespace UploadService.Service.HostedServices
{
    internal class FileWatcherHostedService : IHostedService
    {
        private readonly ILogger<FileWatcherHostedService> _logger;
        private readonly IEventBus _eventBus;
        private readonly IFileSystemWatcher _fileSystemWatcher;
        private readonly IFileUploadEventSequenceFactory _fileUploadEventSequenceFactory;
        private readonly IServiceStateNotificationService _serviceStateNotificationService;

        public FileWatcherHostedService(
            ILogger<FileWatcherHostedService> logger,
            IEventBus eventBus,
            IFileSystemWatcher fileSystemWatcher,
            IFileUploadEventSequenceFactory fileUploadEventSequenceFactory,
            IServiceStateNotificationService serviceStateNotificationService)
        {
            _logger = logger;
            _eventBus = eventBus;
            _fileSystemWatcher = fileSystemWatcher;
            _fileUploadEventSequenceFactory = fileUploadEventSequenceFactory;
            _serviceStateNotificationService = serviceStateNotificationService;
        }

        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Starting {ServiceName}", nameof(FileWatcherHostedService));

            await SubscribeToEventBus();

            _fileSystemWatcher.FileFoundEvent += PublishDocumentUploadEvent;
            await _serviceStateNotificationService.StartAsync(cancellationTokenSource.Token);

            _logger.LogDebug("Started {ServiceName}", nameof(FileWatcherHostedService));
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Stopping {ServiceName}", nameof(FileWatcherHostedService));

            cancellationTokenSource.Cancel();

            await UnsubscribeFromEventBus();

            _fileSystemWatcher.FileFoundEvent -= PublishDocumentUploadEvent;

            _logger.LogDebug("Stopped {ServiceName}", nameof(FileWatcherHostedService));
        }

        private void PublishDocumentUploadEvent(string fullPath)
        {
            var eventSequence = _fileUploadEventSequenceFactory.CreateSequence(File.OpenRead(fullPath));

            foreach (var @event in eventSequence)
            {
                Task.Run(async () =>
                {
                    _logger.LogWarning($"Publishing {@event.SequenceId} file event part #{@event.Index} of {@event.Length}. Path: {fullPath}.");
                    await _eventBus.Publish(@event);
                }).GetAwaiter().GetResult();
            }
        }

        private async Task SubscribeToEventBus()
        {
            await _eventBus.Subscribe<RequestServiceStateEvent, RequestServiceStateEventHandler>();
            await _eventBus.Subscribe<UpdateConfigEvent<WatchConfiguration>, UpdateConfigurationEventHandler<WatchConfiguration>>();
            await _eventBus.Subscribe<UpdateConfigEvent<EventSequenceFactoryConfiguration>, UpdateConfigurationEventHandler<EventSequenceFactoryConfiguration>>();
        }

        private async Task UnsubscribeFromEventBus()
        {
            await _eventBus.Unsubscribe<RequestServiceStateEvent, RequestServiceStateEventHandler>();
            await _eventBus.Unsubscribe<UpdateConfigEvent<WatchConfiguration>, UpdateConfigurationEventHandler<WatchConfiguration>>();
            await _eventBus.Unsubscribe<UpdateConfigEvent<EventSequenceFactoryConfiguration>, UpdateConfigurationEventHandler<EventSequenceFactoryConfiguration>>();
        }
    }
}