﻿using NUnit.Framework;
using PowerManagement.Interop;
using Tests;

namespace PowerManagement.Tests
{
    [TestFixture]
    public sealed class PowerInformationTests : TestBase
    {
        [Test]
        public void GetSystemBatteryState_Ok()
        {
            var result = Manager.GetSystemBatteryState();
            Assert.AreNotEqual(default(SystemBatteryState), result);
            Assert.AreEqual(default(SystemBatteryState), default(SystemBatteryState));
        }

        [Test]
        public void GetSystemPowerInformation_Ok()
        {
            var result = Manager.GetSystemPowerInformation();
            Assert.AreNotEqual(default(SystemPowerInformation), result);
        }
    }
}
