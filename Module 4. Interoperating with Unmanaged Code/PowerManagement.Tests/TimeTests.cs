﻿using NUnit.Framework;
using System;
using Tests;

namespace PowerManagement.Tests
{
    [TestFixture]
    public sealed class TimeTests : TestBase
    {
        [Test]
        public void GetLastSleepTime_Ok()
        {
            var result = Manager.GetLastSleepTime();
            Assert.AreNotEqual(new DateTime(), result);
        }

        [Test]
        public void GetLastWakeTime_Ok()
        {
            var result = Manager.GetLastWakeTime();
            Assert.AreNotEqual(new DateTime(), result);
        }
    }
}
