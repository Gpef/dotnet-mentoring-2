﻿using NUnit.Framework;
using PowerManagement.Interop;

namespace PowerManagement
{
    [TestFixture]
    public sealed class SelfTests
    {
        [Test]
        public void SystemPowerInformation_Default_AreEqual()
        {
            Assert.AreEqual(default(SystemPowerInformation), default(SystemPowerInformation));
        }

        [Test]
        public void SystemBatteryState_Default_AreEqual()
        {
            Assert.AreEqual(default(SystemBatteryState), default(SystemBatteryState));
        }
    }
}