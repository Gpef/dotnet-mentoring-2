﻿namespace PowerManagement
{
    public enum SuspendState
    {
        Sleep,
        Hibernate
    }
}