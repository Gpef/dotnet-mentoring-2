﻿using PowerManagement.Interop;
using System;
using System.Runtime.InteropServices;

namespace PowerManagement
{
    [ComVisible(true)]
    [Guid(ContractGuids.PowerManagerInterfaceGuid)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPowerManager
    {
        bool SetSuspendState(SuspendState state);

        void ReserveHiberationFile();

        void RemoveReservedHibernationFile();

        DateTime GetLastWakeTime();

        DateTime GetLastSleepTime();

        SystemPowerInformation GetSystemPowerInformation();

        SystemBatteryState GetSystemBatteryState();
    }
}