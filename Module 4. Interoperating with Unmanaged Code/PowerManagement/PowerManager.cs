﻿using PowerManagement.Interop;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace PowerManagement
{
    [ComVisible(true)]
    [Guid(ContractGuids.PowerManagerClassGuid)]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerManager : IPowerManager
    {
        public bool SetSuspendState(SuspendState state)
        {
            var isHibernate = state == SuspendState.Hibernate;
            var result = PowerManagementInterop.SetSuspendState(isHibernate, false, false);

            ThrowIfBoolResultIsError(result);

            return result != PowerManagementInterop.BOOL_STATUS_ERROR;
        }

        public void ReserveHiberationFile() => HandleHiberationReservationState(true);
        public void RemoveReservedHibernationFile() => HandleHiberationReservationState(false);

        public DateTime GetLastWakeTime() => GetTimeInformation(PowerInformationLevel.LastWakeTime);
        public DateTime GetLastSleepTime() => GetTimeInformation(PowerInformationLevel.LastSleepTime);

        public SystemPowerInformation GetSystemPowerInformation()
        {
            var result = PowerManagementInterop.CallNtPowerInformation(
                PowerInformationLevel.SystemPowerInformation,
                IntPtr.Zero, 0,
                out SystemPowerInformation outputBuffer,
                Marshal.SizeOf(typeof(SystemPowerInformation)));

            ThrowIfResultIsError(result);

            return outputBuffer;
        }

        public SystemBatteryState GetSystemBatteryState()
        {
            var result = PowerManagementInterop.CallNtPowerInformation(
                PowerInformationLevel.SystemPowerInformation,
                IntPtr.Zero,
                0,
                out SystemBatteryState outputBuffer,
                Marshal.SizeOf(typeof(SystemBatteryState)));

            ThrowIfResultIsError(result);

            return outputBuffer;
        }

        private void HandleHiberationReservationState(bool state)
        {
            var result = PowerManagementInterop.CallNtPowerInformation(
                PowerInformationLevel.SystemReserveHiberFile, state, sizeof(bool), out _, 0);

            ThrowIfResultIsError(result);
        }

        private DateTime GetTimeInformation(PowerInformationLevel level)
        {
            var result = PowerManagementInterop.CallNtPowerInformation(
                level, IntPtr.Zero, 0, out ulong outputBuffer, sizeof(ulong));

            ThrowIfResultIsError(result);

            var convertedTime = UnitConverter.FromHnsOffsetToDateTime(outputBuffer);
            return convertedTime;
        }

        private void ThrowIfResultIsError(uint result)
        {
            if (result != PowerManagementInterop.STATUS_SUCCESS)
            {
                throw new Win32Exception();
            }
        }

        private void ThrowIfBoolResultIsError(uint result)
        {
            if (result == PowerManagementInterop.BOOL_STATUS_ERROR)
            {
                throw new Win32Exception();
            }
        }
    }
}