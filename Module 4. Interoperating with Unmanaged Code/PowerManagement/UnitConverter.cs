﻿using System;

namespace PowerManagement
{
    internal class UnitConverter
    {
        private const int TicksPerNanosecond = 100;

        public static DateTime FromHnsOffsetToDateTime(ulong nanoseconds)
        {
            var ticks = (long)(nanoseconds / TicksPerNanosecond);

            var result = DateTime.UtcNow - TimeSpan.FromTicks(ticks);
            return result;
        }
    }
}