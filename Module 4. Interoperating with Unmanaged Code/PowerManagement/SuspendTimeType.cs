﻿namespace PowerManagement
{
    public enum PowerInformationTimeType
    {
        Sleep,
        Wake
    }
}