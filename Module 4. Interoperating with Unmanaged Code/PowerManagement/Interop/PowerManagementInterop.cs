﻿using System;
using System.Runtime.InteropServices;

namespace PowerManagement.Interop
{
    internal class PowerManagementInterop
    {
        internal const uint STATUS_SUCCESS = 0;
        internal const uint BOOL_STATUS_ERROR = 0;

        #region CallNtPowerInformation

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            PowerInformationLevel InformationLevel,
            [In] IntPtr InputBuffer,
            uint InputBufferLength,
            [Out] IntPtr OutputBuffer,
            uint OutputBufferLength);

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            PowerInformationLevel InformationLevel,
            IntPtr InputBuffer,
            int InputBufferLength,
            [Out] out SystemPowerInformation OutputBuffer,
            int OutputBufferLength);

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            PowerInformationLevel InformationLevel,
            IntPtr InputBuffer,
            int InputBufferLength,
            [Out] out SystemBatteryState OutputBuffer,
            int OutputBufferLength);

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            PowerInformationLevel InformationLevel,
            IntPtr InputBuffer,
            int InputBufferLength,
            [Out] out ulong OutputBuffer,
            int OutputBufferLength);

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            PowerInformationLevel InformationLevel,
            [In] bool InputBuffer,
            int InputBufferLength,
            [Out] out IntPtr OutputBuffer,
            int OutputBufferLength);

        #endregion

        #region SetSuspendState

        /// <summary>
        /// Suspends the system by shutting power down. Depending on the Hibernate parameter, the system either enters a suspend (sleep) state or hibernation (S4).
        /// </summary>
        /// <param name="hibernate">If this parameter is TRUE, the system hibernates. If the parameter is FALSE, the system is suspended.</param>
        /// <param name="forceCritical">Windows Server 2003, Windows XP, and Windows 2000:  If this parameter is TRUE,
        /// the system suspends operation immediately; if it is FALSE, the system broadcasts a PBT_APMQUERYSUSPEND event to each
        /// application to request permission to suspend operation.</param>
        /// <param name="disableWakeEvent">If this parameter is TRUE, the system disables all wake events. If the parameter is FALSE, any system wake events remain enabled.</param>
        /// <returns>If the function succeeds, the return value is true.</returns>
        /// <remarks>See http://msdn.microsoft.com/en-us/library/aa373201(VS.85).aspx </remarks>
        [DllImport("Powrprof.dll", SetLastError = true)]
        public static extern uint SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent);

        #endregion
    }
}