﻿/*
 * 2.	Write a program, which creates a chain of four Tasks.
 * First Task – creates an array of 10 random integer.
 * Second Task – multiplies this array with another random integer.
 * Third Task – sorts this array by ascending.
 * Fourth Task – calculates the average value. All this tasks should print the values to console.
 */

using System;
using System.Linq;
using System.Threading.Tasks;

namespace MultiThreading.Task2.Chaining
{
    public class Program
    {
        private static readonly Random Random = new Random();

        private static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. MultiThreading V1 ");
            Console.WriteLine("2.	Write a program, which creates a chain of four Tasks.");
            Console.WriteLine("First Task – creates an array of 10 random integer.");
            Console.WriteLine("Second Task – multiplies this array with another random integer.");
            Console.WriteLine("Third Task – sorts this array by ascending.");
            Console.WriteLine(
                "Fourth Task – calculates the average value. All this tasks should print the values to console");
            Console.WriteLine();

            var task = CreateTaskChain();
            task.Wait();

            Console.WriteLine("Finished!");
            Console.ReadLine();
        }

        private static Task CreateTaskChain()
        {
            var arrayTask = CreateArray()
                .ContinueWith(prevTask => MultiplyArray(prevTask.Result))
                .ContinueWith(prevTask => SortArray(prevTask.Result.Result))
                .ContinueWith(prevTask => GetAverageValue(prevTask.Result.Result));

            return arrayTask;
        }

        private static Task<int[]> CreateArray()
        {
            var task = new Task<int[]>(() =>
            {
                Console.WriteLine("Array generation is started");

                var array = new int[10];
                for (var index = 0; index < array.Length; index++)
                {
                    array[index] = Random.Next(100);
                    Console.WriteLine($"New #[{index}] array random value generated: {array[index]}");
                }

                Console.WriteLine("Array was generated successfully");
                return array;
            });

            task.Start();
            return task;
        }

        private static Task<int[]> MultiplyArray(int[] array)
        {
            var task = new Task<int[]>(() =>
            {
                Console.WriteLine("Array multiplication is started");

                var multiplyBy = Random.Next(100);
                Console.WriteLine($"Array will be multiplied by {multiplyBy}");

                for (var index = 0; index < array.Length; index++)
                {
                    array[index] = array[index] *= multiplyBy;
                    Console.WriteLine($"Array #[{index}] value after multiplication: {array[index]}");
                }

                Console.WriteLine("Array was multiplied successfully");
                return array;
            });

            task.Start();
            return task;
        }

        private static Task<int[]> SortArray(int[] array)
        {
            var task = new Task<int[]>(() =>
            {
                Console.WriteLine("Array sorting is started");

                Array.Sort(array);

                for (var index = 0; index < array.Length; index++)
                {
                    Console.WriteLine($"Array #[{index}] value after sorting: {array[index]}");
                }

                Console.WriteLine("Array was sorted successfully");

                return array;
            });

            task.Start();
            return task;
        }

        private static Task<double> GetAverageValue(int[] array)
        {
            var task = new Task<double>(() =>
            {
                Console.WriteLine("Array average counting is started");

                var average = array.Average();

                Console.WriteLine($"Average value is {average}");
                Console.WriteLine("Average was found successfully");

                return average;
            });

            task.Start();
            return task;
        }
    }
}