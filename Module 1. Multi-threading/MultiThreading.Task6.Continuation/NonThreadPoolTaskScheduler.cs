using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task6.Continuation
{
    public class NonThreadPoolTaskScheduler : TaskScheduler, IDisposable
    {
        private readonly BlockingCollection<Task> _tasks = new BlockingCollection<Task>();
        private readonly Thread _mainThread;

        public NonThreadPoolTaskScheduler()
        {
            _mainThread = new Thread(new ThreadStart(Execute));

            if (!_mainThread.IsAlive)
            {
                _mainThread.Start();
            }
        }

        private void Execute()
        {
            foreach (var task in _tasks.GetConsumingEnumerable())
            {
                new Thread(() => { TryExecuteTask(task); }).Start();
                if (_tasks.IsAddingCompleted)
                {
                    return;
                }
            }
        }

        protected override IEnumerable<Task> GetScheduledTasks() => _tasks.ToList();

        protected override void QueueTask(Task task)
        {
            if (task != null)
            {
                _tasks.Add(task);
            }
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued) => false;

        private void Dispose(bool disposing)
        {
            if (!disposing) { return; }

            _tasks.CompleteAdding();
            _mainThread.Join();
            _tasks.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}