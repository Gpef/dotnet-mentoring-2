﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task6.Continuation
{
    public class Solution
    {
        public void A()
        {
            Console.WriteLine();
            Console.WriteLine("Solution A");

            CancelledTask().ContinueWith(ShowParentTaskStatus, TaskContinuationOptions.None).Wait();
            Task.CompletedTask.ContinueWith(ShowParentTaskStatus, TaskContinuationOptions.None).Wait();
        }

        public void B()
        {
            Console.WriteLine();
            Console.WriteLine("Solution B");

            try
            {
                var positiveTask = CancelledTask()
                    .ContinueWith(ShowParentTaskStatus, TaskContinuationOptions.NotOnRanToCompletion);
                var negativeTask =
                    Task.CompletedTask.ContinueWith(ShowParentTaskStatus, TaskContinuationOptions.NotOnRanToCompletion);

                Task.WaitAll(positiveTask, negativeTask);
            }
            catch (AggregateException)
            {
                Console.WriteLine($"Task in B was cancelled");
            }
        }

        public void C()
        {
            Console.WriteLine();
            Console.WriteLine("Solution C");

            ThreadPool.SetMinThreads(4, 4);

            for (int i = 0; i <= 3; i++)
            {
                ThreadPool.QueueUserWorkItem(state =>
                {
                    Task.Run(() =>
                    {
                        Console.WriteLine($"Started thread # {Thread.CurrentThread.ManagedThreadId}");
                        while (true)
                        {
                            Thread.Sleep(2000);
                        }
                    });
                });
            }
            Thread.Sleep(1000);
            CancelledTask().ContinueWith(
                    ShowCurrentThreadId,
                    TaskContinuationOptions.NotOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously)
                .Wait();
        }

        public void D()
        {
            Console.WriteLine();
            Console.WriteLine("Solution D");

            try
            {
                using (var taskScheduler = new NonThreadPoolTaskScheduler())
                {
                    CancelledTask().ContinueWith(
                            ShowCurrentThreadId,
                            CancellationToken.None,
                            TaskContinuationOptions.OnlyOnCanceled,
                            taskScheduler)
                        .Wait();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in D: {e.Message}");
            }
        }

        private Task CancelledTask()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var task = Task.Run(() =>
                {
                    Console.WriteLine($"Cancelled task in thread {Thread.CurrentThread.ManagedThreadId}");
                    Task.Delay(100, cancellationTokenSource.Token).Wait(cancellationTokenSource.Token);
                    cancellationTokenSource.Token.ThrowIfCancellationRequested();
                },
                cancellationTokenSource.Token);

            Task.Delay(10).Wait();
            cancellationTokenSource.Cancel();

            return task;
        }

        private Task ShowCurrentThreadId(Task parent)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine($"Current thread id: {threadId}. TaskId: {parent.Id}");
            return Task.FromResult(threadId);
        }

        private Task ShowParentTaskStatus(Task parent)
        {
            Console.WriteLine(
                $"Executed. Parent task successful: {parent.IsCompletedSuccessfully}{Environment.NewLine}" +
                $"Status: {parent.Status}{Environment.NewLine}" +
                $"Exception: {parent.Exception?.Message}{Environment.NewLine}" +
                $"Current thread id: {Thread.CurrentThread.ManagedThreadId}");
            return Task.CompletedTask;
        }
    }
}