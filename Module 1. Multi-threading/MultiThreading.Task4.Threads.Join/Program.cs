﻿/*
 * 4.	Write a program which recursively creates 10 threads.
 * Each thread should be with the same body and receive a state with integer number, decrement it,
 * print and pass as a state into the newly created thread.
 * Use Thread class for this task and Join for waiting threads.
 * 
 * Implement all of the following options:
 * - a) Use Thread class for this task and Join for waiting threads.
 * - b) ThreadPool class for this task and Semaphore for waiting threads.
 */

using System;
using System.Threading;

namespace MultiThreading.Task4.Threads.Join
{
    public class Program
    {
        const int ThreadsCount = 10;

        private static void Main(string[] args)
        {
            Console.WriteLine("4.	Write a program which recursively creates 10 threads.");
            Console.WriteLine(
                "Each thread should be with the same body and receive a state with integer number, decrement it, print and pass as a state into the newly created thread.");
            Console.WriteLine("Implement all of the following options:");
            Console.WriteLine();
            Console.WriteLine("- a) Use Thread class for this task and Join for waiting threads.");
            Console.WriteLine("- b) ThreadPool class for this task and Semaphore for waiting threads.");

            Console.WriteLine();

            SolutionA();
            SolutionB();

            Console.ReadLine();
        }

        #region A

        private static void SolutionA()
        {
            Console.WriteLine("Solution A");

            var state = new IntThreadState(100, ThreadsCount);
            DecrementValueWithJoin(state);
        }

        private static void DecrementValueWithJoin(IntThreadState state)
        {
            if (state.ThreadsLeft < 1)
            {
                return;
            }

            state.ThreadsLeft--;
            state.Value--;
            Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Decremented to {state.Value}");

            var nextThread = new Thread(() => DecrementValueWithJoin(state));
            nextThread.Start();
            nextThread.Join();
        }

        #endregion

        #region B

        private static readonly Semaphore Semaphore = new Semaphore(1, 1);

        private static void SolutionB()
        {
            Console.WriteLine("Solution B");

            var state = new IntThreadState(100, ThreadsCount);
            DecrementValueWithSemaphoreCallback(state);
        }

        private static void DecrementValueWithSemaphoreCallback(object threadState)
        {
            var state = (IntThreadState) threadState;

            if (state.ThreadsLeft < 1)
            {
                return;
            }

            state.ThreadsLeft--;
            state.Value--;
            Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Decremented to {state.Value}");

            Semaphore.WaitOne();
            ThreadPool.QueueUserWorkItem(DecrementValueWithSemaphoreCallback, state);
            Semaphore.Release();
        }

        #endregion
    }
}