namespace MultiThreading.Task4.Threads.Join
{
    public class IntThreadState
    {
        public int Value;
        public int ThreadsLeft;

        public IntThreadState(int value, int threadsLeft)
        {
            Value = value;
            ThreadsLeft = threadsLeft;
        }
    }
}