﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    public class AutoResetEventSolution
    {
        public static Task Run()
        {
            Console.WriteLine(nameof(AutoResetEventSolution));

            var writeHandle = new AutoResetEvent(true);
            var readHandle = new AutoResetEvent(false);

            var collection = new BlockingCollection<int>();

            var printItemsTask = Task.Run(() => PrintItems(collection, writeHandle, readHandle));
            var addTask = Task.Run(() => AddItems(collection, writeHandle, readHandle));

            return Task.WhenAll(printItemsTask, addTask);
        }

        private static void AddItems(BlockingCollection<int> collection, EventWaitHandle writeHandle, EventWaitHandle readHandle)
        {
            Console.WriteLine("Add started");

            for (var i = 0; i < 10; i++)
            {
                writeHandle.WaitOne();

                collection.Add(i);
                Console.WriteLine($"add: {i}");

                readHandle.Set();
            }

            writeHandle.WaitOne();
            collection.CompleteAdding();
            readHandle.Set();

            Console.WriteLine("Add finished");
        }

        private static void PrintItems(BlockingCollection<int> collection, EventWaitHandle writeHandle, EventWaitHandle readHandle)
        {
            Console.WriteLine("Print started");

            while (true)
            {
                readHandle.WaitOne();

                if (collection.IsAddingCompleted)
                {
                    break;
                }

                Console.WriteLine(string.Join(",", collection.ToList()));

                writeHandle.Set();
            }

            Console.WriteLine("Print finished");
        }
    }
}