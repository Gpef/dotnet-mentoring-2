﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    public class MonitorSolution
    {
        private static readonly object CollectionAccessLock = new object();

        public static Task Run()
        {
            Console.WriteLine(nameof(MonitorSolution));

            var collection = new List<int>();
            var cts = new CancellationTokenSource();

            var exitTask = Task.Run(() =>
            {
                Console.WriteLine("Press enter to exit");

                var key = Console.ReadKey();

                if (key.Key == ConsoleKey.Enter)
                {
                    cts.Cancel();
                    lock (CollectionAccessLock)
                    {
                        Console.WriteLine("cts pulse");
                        Monitor.Pulse(CollectionAccessLock);
                    }
                }
            });

            var printItemsTask = Task.Run(() => PrintItems(collection, cts.Token));
            var addTask = Task.Run(() => AddItems(collection));

            return Task.WhenAll(printItemsTask, addTask);
        }

        private static void AddItems(List<int> collection)
        {
            Console.WriteLine("Add started");

            lock (CollectionAccessLock)
            {
                for (var i = 0; i < 10; i++)
                {
                    collection.Add(i);
                    Console.WriteLine($"add: {i}");

                    Console.WriteLine("add pulse");
                    Monitor.Pulse(CollectionAccessLock);
                    Console.WriteLine("add wait");
                    Monitor.Wait(CollectionAccessLock);
                }

                Console.WriteLine("add last pulse");
                Monitor.Pulse(CollectionAccessLock);
            }

            Console.WriteLine("Add finished");
        }

        private static void PrintItems(List<int> collection, CancellationToken cancellationToken)
        {
            Console.WriteLine("Print started");

            while (true)
            {
                Console.WriteLine("print while");

                lock (CollectionAccessLock)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        Monitor.Pulse(CollectionAccessLock);
                        break;
                    }

                    Console.WriteLine(string.Join(",", collection.ToList()));

                    Console.WriteLine("print pulse");
                    Monitor.Pulse(CollectionAccessLock);
                    Console.WriteLine("print wait");
                    Monitor.Wait(CollectionAccessLock);
                }
            }

            Console.WriteLine("Print finished");
        }
    }
}