﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    public class MutexSolution
    {
        public static Task Run()
        {
            Console.WriteLine(nameof(MutexSolution));

            var collection = new BlockingCollection<int>();

            var printItemsTask = Task.Run(() => PrintItems(collection));
            var addTask = Task.Run(() => AddItems(collection));

            return Task.WhenAll(printItemsTask, addTask);
        }

        private static readonly Mutex CollectionAccessMutex = new Mutex();

        private static void AddItems(BlockingCollection<int> collection)
        {
            Console.WriteLine("Add started");

            for (var i = 0; i < 10; i++)
            {
                CollectionAccessMutex.WaitOne();
                collection.Add(i);
                Console.WriteLine($"add: {i}");
                CollectionAccessMutex.ReleaseMutex();
            }

            CollectionAccessMutex.WaitOne();
            collection.CompleteAdding();
            CollectionAccessMutex.ReleaseMutex();
        }

        private static void PrintItems(BlockingCollection<int> collection)
        {
            Console.WriteLine("Print started");

            while (true)
            {
                CollectionAccessMutex.WaitOne();

                if (collection.IsAddingCompleted)
                {
                    break;
                }

                Console.WriteLine(string.Join(",", collection.ToList()));

                CollectionAccessMutex.ReleaseMutex();
            }

            Console.WriteLine("Print finished");
        }
    }
}