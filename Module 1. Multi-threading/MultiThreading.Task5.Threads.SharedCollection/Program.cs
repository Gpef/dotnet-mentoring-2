﻿/*
 * 5. Write a program which creates two threads and a shared collection:
 * the first one should add 10 elements into the collection and the second should print all elements
 * in the collection after each adding.
 * Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.
 */

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("5. Write a program which creates two threads and a shared collection:");
            Console.WriteLine(
                "the first one should add 10 elements into the collection and the second should print all elements in the collection after each adding.");
            Console.WriteLine(
                "Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.");
            Console.WriteLine();


            // Main
            AutoResetEventSolution.Run().Wait();

            //MutexSolution.Run().Wait();
            //MonitorSolution.Run().Wait();

            Console.WriteLine("Press eny key to exit");
            Console.ReadLine();
        }
    }
}