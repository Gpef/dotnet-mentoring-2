﻿using System;
using PostSharp.Aspects;
using PostSharp.Aspects.Advices;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Reflection;
using PostSharp.Serialization;
using System.Diagnostics;

namespace CentralServer.Web.Aspects
{
    [PSerializable]
    public class LoggingAspect : MethodInterceptionAspect, IInstanceScopedAspect
    {
        [ImportMember(new[] { "_logger", "logger", "Logger" }, IsRequired = true)]
        public Property<ILogger> _logger;

        public object CreateInstance(AdviceArgs adviceArgs) => MemberwiseClone();
        public void RuntimeInitializeInstance() { }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            var methodName = args.Method.Name;
            var serializedArguments = SerializeArguments(args);

            var elapsed = ProceedAndGetElapsedTime(args, serializedArguments);
            var returnValue = SerializeReturnValue(args, serializedArguments);

            _logger.Get().LogTrace("Method call '{methodName}' took {elapsed} with arguments {argumentsJson}.{returnValue}",
                methodName, elapsed, serializedArguments, returnValue);
        }

        private string SerializeReturnValue(MethodInterceptionArgs args, string arguments)
        {
            string returnValue;


            if (args.Method is MethodInfo method && method.ReturnType != typeof(void))
            {
                try
                {
                    returnValue = $" Return value: {JsonConvert.SerializeObject(args.ReturnValue)} of type {args.ReturnValue.GetType().Name}.";
                }
                catch (Exception e)
                {
                    returnValue = args.ReturnValue.ToString();
                    _logger.Get().LogError("Error during serializing return value of {methodName} with arguments {arguments} interception: {errorMessage}. ReturnValue string: {returnValue}",
                        args.Method.Name, arguments, e.Message, returnValue);
                }
            }
            else
            {
                returnValue = " (void)";
            }

            return returnValue;
        }

        private TimeSpan ProceedAndGetElapsedTime(MethodInterceptionArgs args, string arguments)
        {
            var timer = new Stopwatch();

            try
            {
                timer.Start();
                args.Proceed();
                timer.Stop();
            }
            catch (Exception e)
            {
                _logger.Get().LogError("Error during {methodName} with arguments {arguments} interception: {errorMessage}.",
                    args.Method.Name, arguments, e.Message);
                throw;
            }

            return new TimeSpan(timer.ElapsedTicks);
        }

        private string SerializeArguments(MethodInterceptionArgs args)
        {
            string argumentsJson;
            try
            {
                argumentsJson = JsonConvert.SerializeObject(args.Arguments);
            }
            catch (Exception e)
            {
                argumentsJson = string.Join(", ", args.Arguments);
                _logger.Get().LogError("Unable to serialize arguments during {methodName} interception: {errorMessage}. Arguments: {argumentsString}",
                    args.Method.Name, e.Message, argumentsJson);
            }

            return argumentsJson;
        }
    }
}