﻿namespace CentralServer.Web.Aspects
{
    public interface ISimpleStringReturnService
    {
        string GetString();

        void SetString(string source);
    }
}