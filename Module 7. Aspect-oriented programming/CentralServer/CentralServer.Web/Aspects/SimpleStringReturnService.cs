﻿using Microsoft.Extensions.Logging;
using System;

namespace CentralServer.Web.Aspects
{
    internal class SimpleStringReturnService : ISimpleStringReturnService
    {
        public ILogger Logger { get; }

        private string _lastString = null;

        public SimpleStringReturnService(ILogger<SimpleStringReturnService> logger)
        {
            Logger = logger;
        }

        [LoggingAspect]
        public string GetString()
        {
            Console.WriteLine("Connsole GetString");
            return _lastString;
        }

        [LoggingAspect]
        public void SetString(string source)
        {
            Console.WriteLine("Connsole SetString");
            _lastString = $"String: {source}";
        }
    }
}