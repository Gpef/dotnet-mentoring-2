﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using CentralServer.Models.Configuration;
using CentralServer.Services.DependencyInjection;
using CentralServer.Services.Events.EventHandlers;
using CentralServer.Services.Events.EventHandlers.FileUpload;
using CentralServer.Web.Aspects;
using EventBus.Abstractions;
using EventBus.DependencyInjection;
using EventBus.Events;
using EventBus.Events.EventSequence;
using EventBus.Events.ServiceState;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CentralService.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public ILifetimeScope AutofacContainer { get; private set; }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IServerConfiguration>(ctx => Configuration.GetSection(nameof(ServerConfiguration)).Get<ServerConfiguration>());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddEventBus(Configuration);
            services.AddTransient<ISimpleStringReturnService, SimpleStringReturnService>();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new ServicesModuleRegister());
            AutofacContainer = builder.Build();

            return new AutofacServiceProvider(AutofacContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });

            SubscribeToEvents(app);
        }

        private void SubscribeToEvents(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<EventSequencePart<FileUploadEvent>, FileUploadEventSequencePartHandler>();
            eventBus.Subscribe<ServiceStateUpdateEvent, ServiceStateUpdateEventHandler>();
        }
    }
}
