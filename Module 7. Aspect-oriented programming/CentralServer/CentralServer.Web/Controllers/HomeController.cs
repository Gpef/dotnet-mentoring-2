﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using CentralService.Web.ViewModels;
using EventBus.Abstractions;
using EventBus.Events;
using Microsoft.Extensions.Logging;
using Common.Configuration;
using System.Linq;
using EventBus.Configuration;
using CentralServer.Services;
using EventBus.Events.ServiceState;
using CentralServer.Web.Aspects;

namespace CentralService.Web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private readonly IEventBus _eventBus;
        private readonly ILogger<HomeController> _logger;
        private readonly IServicesStateManager _servicesStateManager;
        private readonly ISimpleStringReturnService _simpleStringReturnService;

        public HomeController(IEventBus eventBus, ILogger<HomeController> logger, IServicesStateManager servicesStateManager,
            ISimpleStringReturnService simpleStringReturnService)
        {
            _eventBus = eventBus;
            _logger = logger;
            _servicesStateManager = servicesStateManager;
            _simpleStringReturnService = simpleStringReturnService;
        }

        [Route("")]
        public IActionResult Index()
        {
            _simpleStringReturnService.SetString("Index 1");
            _simpleStringReturnService.GetString();

            return View();
        }

        [HttpGet("config/update-watch")]
        public IActionResult UpdateWatchConfig()
        {
            return View();
        }

        [HttpPost("config/update-watch")]
        public IActionResult UpdateWatchConfig(WatchConfiguration config)
        {
            config.Rules = config.Rules.FirstOrDefault()?.Split('\n')?.ToList();
            config.PathsToListen = config.PathsToListen.FirstOrDefault()?.Split('\n')?.ToList();

            var updateWatchConfigEvent = new UpdateConfigEvent<WatchConfiguration>(config);
            _eventBus.Publish(updateWatchConfigEvent);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet("config/update-event-sequence")]
        public IActionResult UpdateEventSequenceConfig()
        {
            return View();
        }

        [HttpPost("config/update-event-sequence")]
        public IActionResult UpdateEventSequenceConfig(EventSequenceFactoryConfiguration config)
        {
            var updateSequenceFactoryConfigEvent = new UpdateConfigEvent<EventSequenceFactoryConfiguration>(config);
            _eventBus.Publish(updateSequenceFactoryConfigEvent);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet("status/update")]
        public IActionResult RequestState()
        {
            _servicesStateManager.SetPollingState();
            _eventBus.Publish(new RequestServiceStateEvent());

            return RedirectToAction(nameof(Status));
        }

        [Route("status")]
        public IActionResult Status()
        {
            return View(_servicesStateManager.States);
        }

        [Route("error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
