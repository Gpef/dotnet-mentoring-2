﻿using System.Collections.Generic;

namespace CentralServer.Web.ViewModels
{
    public class WatchConfigurationFormViewModel
    {
        List<string> PathsToListen { get; }
        List<string> Rules { get; }
    }
}