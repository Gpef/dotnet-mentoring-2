﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using CentralServer.Services.Events.EventHandlers;
using CentralServer.Services.Events.EventHandlers.FileUpload;
using Common.Interceptors;

namespace CentralServer.Services.DependencyInjection
{
    public class ServicesModuleRegister : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileUploadEventSequencePartHandler>();
            builder.RegisterType<ServiceStateUpdateEventHandler>();

            builder.RegisterType<LoggingInterceptor<ServicesStateManager>>();

            builder.RegisterType<ServicesStateManager>().As<IServicesStateManager>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(LoggingInterceptor<ServicesStateManager>));
        }
    }
}