﻿using EventBus.Events.ServiceState;
using System.Collections.Generic;

namespace CentralServer.Services
{
    public interface IServicesStateManager
    {
        Dictionary<string, State> States { get; }

        void SetPollingState();
        void Update(string serviceName, EventBus.Events.ServiceState.State state);
    }
}