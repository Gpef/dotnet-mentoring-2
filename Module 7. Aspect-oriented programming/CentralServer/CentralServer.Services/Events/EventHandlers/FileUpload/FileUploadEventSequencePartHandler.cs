﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CentralServer.Models.Configuration;
using CentralServer.Services.Utils;
using EventBus.Abstractions;
using EventBus.Extensions;
using EventBus.Events;
using EventBus.Events.EventSequence;
using Microsoft.Extensions.Logging;

namespace CentralServer.Services.Events.EventHandlers.FileUpload
{
    public class FileUploadEventSequencePartHandler : IEventHandler<EventSequencePart<FileUploadEvent>>
    {
        private static readonly Dictionary<Guid, List<EventSequencePart<CachedFileUploadEvent>>> _receivedEvents =
            new Dictionary<Guid, List<EventSequencePart<CachedFileUploadEvent>>>();

        private readonly IServerConfiguration _serverConfiguration;
        private readonly ILogger<FileUploadEventSequencePartHandler> _logger;

        public FileUploadEventSequencePartHandler(IServerConfiguration serverConfiguration, ILogger<FileUploadEventSequencePartHandler> logger)
        {
            _serverConfiguration = serverConfiguration;
            _logger = logger;
        }

        public async Task Handle(EventSequencePart<FileUploadEvent> @event)
        {
            if (@event == null) { throw new ArgumentNullException(nameof(@event)); }

            var sequenceId = @event.SequenceId;

            if (!_receivedEvents.ContainsKey(sequenceId))
            {
                _receivedEvents.Add(sequenceId, new List<EventSequencePart<CachedFileUploadEvent>>());
            }

            var sequenceEvents = _receivedEvents[sequenceId];
            var cachedEvent = await CacheSequencePartAsync(@event);
            sequenceEvents.Add(cachedEvent);

            _logger.LogInformation("Event {EventName} #{Index} of {Length} saved.",
                @event.GetType().GetEventNameFromType(), @event.Index, @event.Length);

            if (sequenceEvents.Count == @event.Length)
            {
                await Handle(sequenceEvents, @event.Payload.FileName);
            }
        }

        private async Task Handle(IEnumerable<EventSequencePart<CachedFileUploadEvent>> eventSequence, string destFileName)
        {
            var destFilePath = Path.GetFullPath(FileNameUtils.GetNextAvailableFilename(Path.Combine(_serverConfiguration.FilesFolderPath, destFileName)));

            FileUtils.CreateParentDirectoryIfNeeded(destFilePath);

            using (var destinationFileStream = File.OpenWrite(destFilePath))
            {
                foreach (var eventPart in eventSequence.OrderBy(e => e.Index))
                {
                    await FileUtils.ComulativeCopyAsync(eventPart.Payload.FileBytesPath, destinationFileStream);
                }
            }

            _logger.LogInformation("Event sequence {SequenceId} for file {FilePath} handled.", eventSequence.First().SequenceId, destFilePath);
        }

        private async Task<EventSequencePart<CachedFileUploadEvent>> CacheSequencePartAsync(EventSequencePart<FileUploadEvent> @event)
        {
            var eventContent = @event.Payload;

            var tempPath = Path.GetFullPath(BuildTempFilePath(@event.SequenceId, @event.Index, eventContent.FileName));
            await FileUtils.OverriteCopyAsync(eventContent.FileBytes.ToArray(), tempPath);

            var cachedEvent = new CachedFileUploadEvent(eventContent.FileName, tempPath);
            var cachedSequencePart = new EventSequencePart<CachedFileUploadEvent>(
                @event.SequenceId, @event.Index, @event.Length, cachedEvent);

            return cachedSequencePart;
        }

        private string BuildTempFilePath(Guid sequenceId, int fileIndex, string fileName)
        {
            var now = DateTime.UtcNow;

            return Path.Combine(
                _serverConfiguration.TempFilesFolderPath,
                now.Year.ToString(),
                now.Month.ToString(),
                now.Day.ToString(),
                sequenceId.ToString("N"),
                $"{fileName}{_serverConfiguration.TempFileExtension}_{fileIndex}");
        }
    }
}