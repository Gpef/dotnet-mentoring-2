﻿namespace Common.Configuration
{
    public interface IAppConfiguration
    {
        string ServiceName { get; }
    }
}