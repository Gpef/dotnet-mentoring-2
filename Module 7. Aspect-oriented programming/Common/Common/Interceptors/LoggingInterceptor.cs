﻿using Castle.DynamicProxy;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Common.Interceptors
{
    public class LoggingInterceptor<T> : IInterceptor
    {
        private readonly ILogger<T> _logger;

        public LoggingInterceptor(ILogger<T> logger)
        {
            _logger = logger;
        }

        public void Intercept(IInvocation invocation)
        {
            var serializedArguments = SerializeArguments(invocation);
            var elapsed = ProceedAndGetElapsedTime(invocation, serializedArguments);

            if (IsAsyncMethod(invocation))
            {
                InterceptAsync((Task)invocation.ReturnValue).GetAwaiter().GetResult();
            }

            var returnValue =  SerializeReturnValue(invocation, serializedArguments);

            _logger.LogTrace("Method call '{methodName}' took {elapsed} with arguments {argumentsJson}.{returnValue}",
                invocation.Method.Name, elapsed, serializedArguments, returnValue);
        }

        private static bool IsAsyncMethod(IInvocation invocation) => 
            invocation.MethodInvocationTarget.GetCustomAttribute(typeof(AsyncStateMachineAttribute)) != null &&
            typeof(Task).IsAssignableFrom(invocation.MethodInvocationTarget.ReturnType);

        private async Task InterceptAsync(Task task)
        {
            try
            {
                await task.ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _logger.LogError("Error awaiting for async method interception. {message}", e.Message);
                throw;
            }
        }

        private string SerializeReturnValue(IInvocation invocation, string arguments)
        {
            string returnValue;

            if (invocation.Method.ReturnType == typeof(void) || 
                IsAsyncMethod(invocation) && !invocation.Method.ReturnType.IsGenericType)
            {
                returnValue = " No return value (void).";
            }
            else
            {
                try
                {
                    object valueToSerialize;
                    Type returnValueType;

                    if (IsAsyncMethod(invocation))
                    {
                        valueToSerialize = ((dynamic)invocation.ReturnValue).Result;
                        returnValueType = invocation.Method.ReturnType.GenericTypeArguments[0];
                    }
                    else
                    {
                        valueToSerialize = invocation.ReturnValue;
                        returnValueType = invocation.ReturnValue.GetType();
                    }

                    returnValue = $" Return value: {JsonConvert.SerializeObject(valueToSerialize)} of type {returnValueType.Name}.";
                }
                catch (Exception e)
                {
                    returnValue = invocation.ReturnValue.ToString();

                    _logger.LogError(
                        "Error during return value serialization of {methodName} with arguments {arguments} interception: {errorMessage}. Return Value string: {returnValue}",
                        invocation.Method.Name, arguments, e.Message, returnValue);
                }
            }

            return returnValue;
        }

        private TimeSpan ProceedAndGetElapsedTime(IInvocation invocation, string arguments)
        {
            var timer = new Stopwatch();

            try
            {
                timer.Start();
                invocation.Proceed();
                timer.Stop();
            }
            catch (Exception e)
            {
                _logger.LogError("Error during {methodName} with arguments {arguments} interception: {errorMessage}.",
                    invocation.Method.Name, arguments, e.Message);
                throw;
            }

            return new TimeSpan(timer.ElapsedTicks);
        }

        private string SerializeArguments(IInvocation invocation)
        {
            string argumentsJson;

            try
            {
                argumentsJson = JsonConvert.SerializeObject(invocation.Arguments);
            }
            catch (Exception e)
            {
                argumentsJson = string.Join(", ", invocation.Arguments);

                _logger.LogError(
                    "Unable to serialize arguments during {methodName} interception: {errorMessage}. Arguments: {argumentsString}",
                    invocation.Method.Name, e.Message, argumentsJson);
            }

            return argumentsJson;
        }
    }
}