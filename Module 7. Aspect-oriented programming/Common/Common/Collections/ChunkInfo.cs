﻿namespace Common.Collections
{
    public struct ChunkInfo
    {
        public int FullChunksCount { get; }
        public int LastChunkLength { get; }

        public ChunkInfo(int fullChunksCount, int lastChunkLength)
        {
            FullChunksCount = fullChunksCount;
            LastChunkLength = lastChunkLength;
        }
    }
}