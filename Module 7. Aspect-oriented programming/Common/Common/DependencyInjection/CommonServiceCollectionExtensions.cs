﻿using Common.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.DependencyInjection
{
    public static class CommonServiceCollectionExtensions
    {
        public static IServiceCollection AddReloadableWatchConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IReloadableWatchConfiguration>(ctx => configuration.GetSection(nameof(WatchConfiguration)).Get<WatchConfiguration>());
            services.AddSingleton<IWatchConfiguration>(ctx => ctx.GetRequiredService<IReloadableWatchConfiguration>());

            return services;
        }

        public static IServiceCollection AddAppConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IAppConfiguration>(ctx => configuration.GetSection(nameof(AppConfiguration)).Get<AppConfiguration>());

            return services;
        }
    }
}