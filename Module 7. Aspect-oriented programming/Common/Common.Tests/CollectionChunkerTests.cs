using Common.Collections;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class CollectionChunkerTests
    {
        [Test]
        [TestCase(11, 3, 3, 2)]
        [TestCase(4, 2, 2, 0)]
        [TestCase(20, 6, 3, 2)]
        public void GetChunckInfo(int collectionSize, int stepSize, int expectedChuncksCount, int expectedLastChunkLength)
        {
            var chunkInfo = CollectionChunkHelper.GetChunckInfo(collectionSize, stepSize);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedChuncksCount, chunkInfo.FullChunksCount);
                Assert.AreEqual(expectedLastChunkLength, chunkInfo.LastChunkLength);
            });
        }
    }
}