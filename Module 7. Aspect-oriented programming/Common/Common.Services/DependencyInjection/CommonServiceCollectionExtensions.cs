﻿using Common.Services.Services.ServiceState;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Services.DependencyInjection
{
    public static class CommonServiceCollectionExtensions
    {
        public static IServiceCollection AddCommonServices(this IServiceCollection services)
        {
            services.AddSingleton<IServiceStateAccessor, ServiceStateAccessor>();
            services.AddScoped<IServiceStateNotificationService, ServiceStateNotificationService>();

            return services;
        }
    }
}