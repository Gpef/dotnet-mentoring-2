﻿using EventBus.Events.ServiceState;
using System;

namespace Common.Services.Services.ServiceState
{
    public interface IServiceStateAccessor
    {
        State CurrentState { get; }

        void AddProcessing(Guid processingId);

        void RemoveProcessing(Guid processingId);
    }
}