﻿using System.Threading;
using System.Threading.Tasks;

namespace Common.Services.Services.ServiceState
{
    public interface IServiceStateNotificationService
    {
        Task NotifyAsync();
        Task StartAsync(CancellationToken cancellationToken);
    }
}
