﻿using EventBus.Abstractions;
using EventBus.Events.ServiceState;
using System.Threading;
using System.Threading.Tasks;
using Common.Configuration;

namespace Common.Services.Services.ServiceState
{
    internal class ServiceStateNotificationService : IServiceStateNotificationService
    {
        private const int IdleTimeoutMilliseconds = 60_000;

        private readonly IEventBus _eventBus;
        private readonly IAppConfiguration _appConfiguration;
        private readonly IServiceStateAccessor _serviceStateAccessor;

        public ServiceStateNotificationService(IEventBus eventBus, IAppConfiguration appConfiguration, IServiceStateAccessor serviceStateAccessor)
        {
            _eventBus = eventBus;
            _appConfiguration = appConfiguration;
            _serviceStateAccessor = serviceStateAccessor;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                await NotifyAsync();
                await Task.Delay(IdleTimeoutMilliseconds);
            }
        }

        public async Task NotifyAsync()
        {
            var stateEvent = new ServiceStateUpdateEvent(_appConfiguration.ServiceName, _serviceStateAccessor.CurrentState);
            await _eventBus.Publish(stateEvent);
        }
    }
}