﻿using EventBus.Abstractions;
using EventBus.AzureServiceBus;
using EventBus.Configuration;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace EventBus.DependencyInjection
{
    public static class EventBusServiceCollectionExtensions
    {
        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration(configuration);

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            services.AddSingleton<IServiceBusPersistentConnection>(ctx =>
            {
                var config = ctx.GetRequiredService<IServiceBusConfiguration>();

                var connectionBuilder = new ServiceBusConnectionStringBuilder(config.ConnectionString);
                var connection = new DefaultServiceBusPersistentConnection(connectionBuilder, config);

                return connection;
            });

            services.AddSingleton<IEventBus>(ctx =>
            {
                var config = ctx.GetService<IServiceBusConfiguration>();

                var connection = ctx.GetRequiredService<IServiceBusPersistentConnection>();
                var logger = ctx.GetRequiredService<ILogger<EventBusServiceBus>>();
                var subsManager = ctx.GetRequiredService<IEventBusSubscriptionsManager>();
                var serviceProvider = ctx.GetRequiredService<IServiceProvider>();

                var eventBus = new EventBusServiceBus(connection, logger, subsManager, serviceProvider, config);

                return eventBus;
            });

            return services;
        }

        private static IServiceCollection AddConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IServiceBusConfiguration>(ctx => configuration.GetSection(nameof(ServiceBusConfiguration)).Get<ServiceBusConfiguration>());

            services.AddSingleton<IReloadableEventSequenceFactoryConfiguration>(ctx => configuration.GetSection(nameof(EventSequenceFactoryConfiguration)).Get<EventSequenceFactoryConfiguration>());
            services.AddSingleton<IEventSequenceFactoryConfiguration>(ctx => ctx.GetRequiredService<IReloadableEventSequenceFactoryConfiguration>());

            return services;
        }
    }
}