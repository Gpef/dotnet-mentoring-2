﻿using EventBus.Events.Abstractions;
using System.Threading.Tasks;

namespace EventBus.Abstractions
{
    public interface IEventBus
    {
        Task Publish<TEvent>(TEvent @event) where TEvent : Event;

        void SubscribeDynamic<TEventHandler>(string eventName) where TEventHandler : IDynamicEventHandler;

        void UnsubscribeDynamic<TEventHandler>(string eventName) where TEventHandler : IDynamicEventHandler;

        Task Subscribe<TEvent, TEventHandler>() where TEvent : Event where TEventHandler : IEventHandler<TEvent>;

        Task Unsubscribe<TEvent, TEventHandler>() where TEventHandler : IEventHandler<TEvent> where TEvent : Event;
    }
}