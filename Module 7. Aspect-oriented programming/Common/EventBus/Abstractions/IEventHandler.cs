﻿using EventBus.Events.Abstractions;
using System.Threading.Tasks;

namespace EventBus.Abstractions
{
    public interface IEventHandler<in TEvent> : IEventHandler
        where TEvent : Event
    {
        Task Handle(TEvent @event);
    }

    public interface IEventHandler { }
}