﻿using EventBus.Events.Abstractions;
using EventBus.Events.EventBusEntity;

namespace EventBus.Events
{
    [EventBusEntity(EventBusEntityType.Topic, "service-topic")]
    public class UpdateConfigEvent<TConfiguration> : EventWithPayload<TConfiguration>
    {
        public UpdateConfigEvent(TConfiguration payload) : base(payload) { }
    }
}