﻿using EventBus.Events.Abstractions;
using EventBus.Events.EventBusEntity;

namespace EventBus.Events.ServiceState
{
    [EventBusEntity(EventBusEntityType.Topic, "service-topic")]
    public class RequestServiceStateEvent : Event
    {

    }
}