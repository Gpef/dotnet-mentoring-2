﻿using EventBus.Events.Abstractions;
using EventBus.Events.EventBusEntity;

namespace EventBus.Events
{
    [EventBusEntity(EventBusEntityType.Queue, "file-upload")]
    public class FileUploadEvent : Event
    {
        public byte[] FileBytes { get; set; }
        public string FileName { get; set; }

        public FileUploadEvent(string fileName, byte[] fileBytes)
        {
            FileName = fileName;
            FileBytes = fileBytes;
        }
    }
}