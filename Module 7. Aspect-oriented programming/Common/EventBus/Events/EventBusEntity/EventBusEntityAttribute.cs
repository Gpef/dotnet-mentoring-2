﻿using System;

namespace EventBus.Events.EventBusEntity
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class EventBusEntityAttribute : Attribute
    {
        public EventBusEntityInfo EntityInfo { get; }

        public EventBusEntityAttribute(EventBusEntityType type, string name)
        {
            EntityInfo = new EventBusEntityInfo(type, name);
        }
    }
}