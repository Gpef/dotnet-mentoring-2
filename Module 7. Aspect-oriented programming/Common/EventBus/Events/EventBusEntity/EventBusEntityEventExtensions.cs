﻿using EventBus.Events.Abstractions;
using System;
using System.Linq;
using System.Reflection;

namespace EventBus.Events.EventBusEntity
{
    public static class EventBusEntityEventExtensions
    {
        public static EventBusEntityAttribute GetEntityAttribute<TEvent>(this TEvent @event) where TEvent : Event => typeof(TEvent).GetEntityAttribute();

        public static EventBusEntityAttribute GetEntityAttribute(this Type eventType)
        {
            var attribute = eventType.GetCustomAttribute<EventBusEntityAttribute>();

            if (attribute == null && eventType.IsGenericType)
            {
                var argumentType = eventType.GenericTypeArguments.First();
                attribute = argumentType.GetCustomAttribute<EventBusEntityAttribute>();
            }

            return attribute;
        }
    }
}
