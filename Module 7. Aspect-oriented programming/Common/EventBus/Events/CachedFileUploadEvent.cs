﻿using EventBus.Events.Abstractions;

namespace EventBus.Events
{
    public class CachedFileUploadEvent : Event
    {
        public string FileBytesPath { get; set; }
        public string FileName { get; set; }

        public CachedFileUploadEvent(string fileName, string fileBytesPath)
        {
            FileName = fileName;
            FileBytesPath = fileBytesPath;
        }
    }
}