﻿namespace EventBus.Configuration
{
    public class EventSequenceFactoryConfiguration : IReloadableEventSequenceFactoryConfiguration
    {
        public int PartSize { get; set; } = 5;

        public void SetPartSize(int partSize)
        {
            PartSize = partSize;
        }
    }
}