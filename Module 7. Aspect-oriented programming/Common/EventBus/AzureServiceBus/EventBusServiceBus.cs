﻿using EventBus.Abstractions;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Azure.ServiceBus.Management;
using EventBus.Extensions;
using Microsoft.Azure.ServiceBus.Core;
using EventBus.Events.Abstractions;
using System.Collections.Generic;
using EventBus.Events.EventBusEntity;
using System.Linq;

namespace EventBus.AzureServiceBus
{
    internal class EventBusServiceBus : IEventBus
    {
        private readonly IServiceBusPersistentConnection _serviceBusPersistentConnection;
        private readonly ILogger<EventBusServiceBus> _logger;
        private readonly IEventBusSubscriptionsManager _subscriptionsManager;
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceBusConfiguration _serviceBusConfiguration;

        private readonly Dictionary<string, SubscriptionClient> _subscriptionClients;

        public EventBusServiceBus(
            IServiceBusPersistentConnection serviceBusPersisterConnection,
            ILogger<EventBusServiceBus> logger,
            IEventBusSubscriptionsManager subsManager,
            IServiceProvider serviceProvider,
            IServiceBusConfiguration serviceBusConfiguration)
        {
            _serviceBusPersistentConnection = serviceBusPersisterConnection;
            _logger = logger;
            _subscriptionsManager = subsManager;
            _serviceBusConfiguration = serviceBusConfiguration;
            _serviceProvider = serviceProvider;

            CreateSubscriptionEntitiesIfNotExists(_serviceBusConfiguration).GetAwaiter().GetResult();
        }

        public async Task Publish<TEvent>(TEvent @event) where TEvent : Event
        {
            if (@event == null) { throw new ArgumentNullException(nameof(@event)); }

            var eventName = @event.GetType().GetEventNameFromType();
            var jsonMessage = JsonConvert.SerializeObject(@event);
            var body = Encoding.UTF8.GetBytes(jsonMessage);

            var message = new Message
            {
                MessageId = Guid.NewGuid().ToString(),
                Body = body,
                Label = eventName,
            };

            ISenderClient serviceBusClient = _serviceBusPersistentConnection.CreateSenderClient(@event.GetEntityAttribute().EntityInfo);

            await serviceBusClient.SendAsync(message);

            _logger.LogInformation($"Event published {eventName}");
        }

        public void SubscribeDynamic<TEventHandler>(string eventName) where TEventHandler : IDynamicEventHandler
        {
            if (string.IsNullOrEmpty(eventName)) { throw new ArgumentException("Event name cannot be null or empty.", nameof(eventName)); }

            _logger.LogInformation("Subscribing to dynamic event {EventName} with {EventHandler}", eventName, typeof(TEventHandler).Name);

            _subscriptionsManager.AddDynamicSubscription<TEventHandler>(eventName);
        }

        private async Task SubscribeToTopicAsync(string topicName, string eventName)
        {
            try
            {
                var subscripbtionClient = _serviceBusPersistentConnection.CreateSubscriptionClient(topicName);

                var rules = await subscripbtionClient.GetRulesAsync();

                if (rules.All(r => r.Name != eventName))
                {
                    await subscripbtionClient.AddRuleAsync(new RuleDescription
                    {
                        Filter = new CorrelationFilter { Label = eventName },
                        Name = eventName
                    });
                }

                RegisterReceiverClientMessageHandlers(subscripbtionClient);
            }
            catch (ServiceBusException e)
            {
                _logger.LogError(e, "Error during rule {EventName} adding.", eventName);
            }
        }

        private async Task UnsubscribeFromTopicAsync(string topicName, string eventName)
        {
            try
            {
                var subscriptionClient = _serviceBusPersistentConnection.CreateSubscriptionClient(topicName);
                await subscriptionClient.RemoveRuleAsync(eventName);
            }
            catch (ServiceBusException e)
            {
                _logger.LogError(e, "Error during rule {EventName} removeing.", eventName);
            }
        }

        public async Task Subscribe<TEvent, TEventHandler>() where TEvent : Event where TEventHandler : IEventHandler<TEvent>
        {
            var eventName = typeof(TEvent).GetEventNameFromType();

            var containsKey = _subscriptionsManager.HasSubscriptionsForEvent<TEvent>();
            if (!containsKey)
            {
                var entityInfo = typeof(TEvent).GetEntityAttribute().EntityInfo;

                switch (entityInfo.Type)
                {
                    case EventBusEntityType.Topic:
                        await SubscribeToTopicAsync(entityInfo.Name, eventName);
                        break;
                    case EventBusEntityType.Queue:
                        RegisterReceiverClientMessageHandlers(_serviceBusPersistentConnection.CreateQueueClient(entityInfo.Name));
                        break;
                }
            }

            _subscriptionsManager.AddSubscription<TEvent, TEventHandler>();
            _logger.LogInformation("Subscribed to event {EventName} with {EventHandler}", eventName, typeof(TEventHandler).Name);
        }

        public async Task Unsubscribe<TEvent, TEventHandler>() where TEvent : Event where TEventHandler : IEventHandler<TEvent>
        {
            var eventName = typeof(TEvent).GetEventNameFromType();

            var entityInfo = typeof(TEvent).GetEntityAttribute().EntityInfo;

            if (entityInfo.Type == EventBusEntityType.Topic)
            {
                await UnsubscribeFromTopicAsync(entityInfo.Name, eventName);
            }

            _subscriptionsManager.RemoveSubscription<TEvent, TEventHandler>();
            _logger.LogInformation("Unsubscribed from event {EventName}", eventName);
        }

        public void UnsubscribeDynamic<TEventHandler>(string eventName) where TEventHandler : IDynamicEventHandler
        {
            if (string.IsNullOrEmpty(eventName)) { throw new ArgumentException("Event name cannot be null or empty.", nameof(eventName)); }

            _logger.LogInformation("Unsubscribing from dynamic event {EventName}", eventName);

            _subscriptionsManager.RemoveDynamicSubscription<TEventHandler>(eventName);
        }

        public void Dispose()
        {
            _subscriptionsManager.Clear();
        }

        private async Task CreateSubscriptionEntitiesIfNotExists(IServiceBusConfiguration config)
        {
            var client = new ManagementClient(config.ConnectionString);

            foreach (var topic in config.Topics)
            {
                if (!await client.SubscriptionExistsAsync(topic, config.ClientName))
                {
                    await client.CreateSubscriptionAsync(new SubscriptionDescription(topic, config.ClientName));
                }
            }
        }

        private void RegisterReceiverClientMessageHandlers(IReceiverClient client)
        {
            try
            {
                client.RegisterMessageHandler(
               async (message, cancellationToken) =>
               {
                   var eventName = message.Label;
                   var messageData = Encoding.UTF8.GetString(message.Body);

                   if (await ProcessEvent(eventName, messageData))
                   {
                       await client.CompleteAsync(message.SystemProperties.LockToken);
                       _logger.LogInformation("Event handled: {EventLabel}.", message.Label);
                   }
               },
               new MessageHandlerOptions(ExceptionReceivedHandler) { MaxConcurrentCalls = 10, AutoComplete = false });
            }
            catch (InvalidOperationException e)
            {
                if (e.Message == "A message handler has already been registered.")
                {
                    _logger.LogDebug("A message handler has already been registered for {ClientId}", client.ClientId);
                }
                else throw;
            }
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            var exception = exceptionReceivedEventArgs.Exception;
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;

            _logger.LogError(exception, "ERROR handling message: {ExceptionMessage} - Context: {@ExceptionContext}", exception.Message, context);

            return Task.CompletedTask;
        }

        private async Task<bool> ProcessEvent(string eventName, string message)
        {
            var processed = false;

            if (_subscriptionsManager.HasSubscriptionsForEvent(eventName))
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var subscriptions = _subscriptionsManager.GetHandlersForEvent(eventName);
                    foreach (var subscription in subscriptions)
                    {
                        if (subscription.IsDynamic)
                        {
                            if (!(scope.ServiceProvider.GetService(subscription.HandlerType) is IDynamicEventHandler handler)) continue;
                            dynamic eventData = JObject.Parse(message);
                            await handler.Handle(eventData);
                        }
                        else
                        {
                            var handler = scope.ServiceProvider.GetService(subscription.HandlerType);
                            if (handler == null)
                            {
                                _logger.LogWarning("Handler {HandlerType} wasn't found", subscription.HandlerType.FullName);
                                continue;
                            }

                            var eventType = _subscriptionsManager.GetEventTypeByName(eventName);
                            var @event = JsonConvert.DeserializeObject(message, eventType);
                            var concreteType = typeof(IEventHandler<>).MakeGenericType(eventType);
                            await (Task)concreteType
                                .GetMethod(nameof(IEventHandler<Event>.Handle))
                                .Invoke(handler, new object[] { @event });
                        }
                    }
                }

                processed = true;
            }

            return processed;
        }
    }
}
