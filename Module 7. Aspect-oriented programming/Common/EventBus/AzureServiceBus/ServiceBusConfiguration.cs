﻿using System.Collections.Generic;

namespace EventBus.AzureServiceBus
{
    internal class ServiceBusConfiguration : IServiceBusConfiguration
    {
        public string ConnectionString { get; set; }

        public string ClientName { get; set; }

        public List<string> Queues { get; set; } = new List<string>();
        public List<string> Topics { get; set; } = new List<string>();
    }
}