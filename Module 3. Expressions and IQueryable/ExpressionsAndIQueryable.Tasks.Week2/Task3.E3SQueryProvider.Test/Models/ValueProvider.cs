namespace Task3.E3SQueryProvider.Test.Models
{
    public class ValueProvider
    {
        public object ObjectProperty { get; set; }
        public object ObjectField;

        public ValueProvider(object property = null, object field = null)
        {
            ObjectProperty = property;
            ObjectField = field;
        }

        public object GetFirstNonNullValueOrDefault()
        {
            if (ObjectProperty != null)
            {
                return ObjectProperty;
            }

            if (ObjectField != null)
            {
                return ObjectField;
            }

            return null;
        }
    }
}