namespace Task3.E3SQueryProvider.Test.Models
{
    public class ComplexValueProvider
    {
        public ValueProvider PropertyProvider { get; set; }
        public ValueProvider FieldProvider;

        public ComplexValueProvider(ValueProvider property = null, ValueProvider field = null)
        {
            PropertyProvider = property;
            FieldProvider = field;
        }
    }
}