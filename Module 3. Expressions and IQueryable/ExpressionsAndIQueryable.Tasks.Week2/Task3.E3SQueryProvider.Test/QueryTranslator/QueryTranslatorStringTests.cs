using System;
using System.Linq.Expressions;
using NUnit.Framework;
using Task3.E3SQueryProvider.Models.Entitites;
using Task3.E3SQueryProvider.Test.Models;

namespace Task3.E3SQueryProvider.Test.QueryTranslator
{
    [TestFixture]
    public class RequestTranslatorStringTests : QueryTranslatorTestBase
    {
        #region SubTask 2: inclusion operations

        [Test]
        public void TestMethodEqualsStringWithConst()
        {
            Expression<Func<EmployeeEntity, bool>> expression = employee => employee.Workstation.Equals("EPRUIZHW006");

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006)", translated);
        }

        [Test]
        public void TestStartsWithWithConst()
        {
            Expression<Func<EmployeeEntity, bool>> expression = employee =>
                employee.Workstation.StartsWith("EPRUIZHW006");

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006*)", translated);
        }

        [Test]
        public void TestEndsWithWithConst()
        {
            Expression<Func<EmployeeEntity, bool>> expression = employee => employee.Workstation.EndsWith("IZHW0060");

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(*IZHW0060)", translated);
        }

        [Test]
        public void TestContainsWithConst()
        {
            Expression<Func<EmployeeEntity, bool>> expression = employee => employee.Workstation.Contains("IZHW006");

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(*IZHW006*)", translated);
        }

        #endregion

        //[Test]
        public void TestMethodEqualsStringWithMember()
        {
            var values = new ComplexValueProvider(new ValueProvider("EPRUIZHW006"));
            Expression<Func<EmployeeEntity, bool>> expression = employee =>
                employee.Workstation.Equals(values.PropertyProvider.ObjectProperty.ToString());

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006)", translated);
        }
    }
}