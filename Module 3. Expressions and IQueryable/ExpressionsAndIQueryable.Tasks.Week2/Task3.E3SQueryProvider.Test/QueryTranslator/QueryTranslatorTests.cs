﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;
using Task3.E3SQueryProvider.Models.Entitites;

namespace Task3.E3SQueryProvider.Test.QueryTranslator
{
    [TestFixture]
    public class FTSRequestTranslatorTests : QueryTranslatorTestBase
    {
        #region SubTask 1 : operands order

        [Test]
        public void TestBinaryEquals()
        {
            Expression<Func<EmployeeEntity, bool>> expression
                = employee => employee.Workstation == "EPRUIZHW006";

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006)", translated);
        }

        [Test]
        public void TestBinaryEqualsBackOrder()
        {
            Expression<Func<EmployeeEntity, bool>> expression
                = employee => "EPRUIZHW006" == employee.Workstation;

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006)", translated);
        }

        [Test]
        public void TestBinaryEqualsQueryable()
        {
            Expression<Func<IQueryable<EmployeeEntity>, IQueryable<EmployeeEntity>>> expression
                = query => query.Where(e => e.Workstation == "EPRUIZHW006");

            var translated = Translate(expression);
            Assert.AreEqual("Workstation:(EPRUIZHW006)", translated);
        }

        #endregion

        [Test]
        public void TestObjectEquals()
        {
            object value = 1L;

            Expression<Func<IQueryable<EmployeeEntity>, IQueryable<EmployeeEntity>>> expression
                = query => query.Where(e => e.Workstation.Equals(1L));

            var translated = Translate(expression);
            Assert.AreEqual($"Workstation:({1L})", translated);
        }
    }
}