using System.Linq.Expressions;
using Task3.E3SQueryProvider.ExpressionVisitors;

namespace Task3.E3SQueryProvider.Test.QueryTranslator
{
    public abstract class QueryTranslatorTestBase
    {
        protected string Translate(Expression expression)
        {
            var translator = new FTSQueryExpressionTranslator(expression);
            return translator.Translate();
        }
    }
}