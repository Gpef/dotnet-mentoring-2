﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Task3.E3SQueryProvider.Models.Request.Enums;

namespace Task3.E3SQueryProvider.Models.Request
{
    [JsonDictionary]
    public class SortingCollection : Dictionary<string, SortOrder> { }
}
