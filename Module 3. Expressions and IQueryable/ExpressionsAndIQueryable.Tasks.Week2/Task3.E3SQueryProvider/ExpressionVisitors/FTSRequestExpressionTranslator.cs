using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ExpressionToString;
using Task3.E3SQueryProvider.Models.Request;

namespace Task3.E3SQueryProvider.ExpressionVisitors
{
    public class FTSRequestExpressionTranslator : ExpressionVisitor
    {
        private readonly List<string> _resultQueries;

        public FTSRequestExpressionTranslator()
        {
            _resultQueries = new List<string>();
        }

        public List<string> Translate(Expression expression)
        {
            Console.WriteLine($"Translating {expression.ToString("C#")}");

            if (_resultQueries.Count > 0)
            {
                throw new InvalidOperationException(
                    $"Queries are not empty. {nameof(FTSRequestExpressionTranslator)} was already used.");
            }

            Visit(expression);

            return _resultQueries;
        }

        public FTSQueryRequest TranslateAsRequest(Expression expression)
        {
            var queries = Translate(expression);
            var statements = new List<Statement>();

            foreach (var query in queries)
            {
                statements.Add(new Statement {Query = query});
            }

            return new FTSQueryRequest {Statements = statements};
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.AndAlso:
                    VisitAndAlsoBinaryExpression(node);
                    break;

                default:
                    throw new NotSupportedException($"Expression: {node}. Operation {node.NodeType} is not supported");
            }

            return node;
        }

        private Expression VisitAndAlsoBinaryExpression(BinaryExpression node)
        {
            VisitIfBinaryOrVisitQuery(node.Left);
            VisitIfBinaryOrVisitQuery(node.Right);

            return node;
        }

        private Expression VisitIfBinaryOrVisitQuery(Expression expression)
        {
            if (expression.NodeType == ExpressionType.AndAlso)
            {
                return VisitBinary((BinaryExpression) expression);
            }

            var queryVisitor = new FTSQueryExpressionTranslator(expression);
            _resultQueries.Add(queryVisitor.Translate());

            return expression;
        }
    }
}