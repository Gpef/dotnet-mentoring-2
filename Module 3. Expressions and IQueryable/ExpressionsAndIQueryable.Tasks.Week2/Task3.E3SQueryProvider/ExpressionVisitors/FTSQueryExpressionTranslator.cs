﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ExpressionToString;

namespace Task3.E3SQueryProvider.ExpressionVisitors
{
    public class FTSQueryExpressionTranslator : ExpressionVisitor
    {
        private const string OpenBracket = "(";
        private const string CloseBracket = ")";
        private const string AsteriskWildcard = "*";

        private readonly StringBuilder _resultStringBuilder;
        private readonly Expression _expression;

        public FTSQueryExpressionTranslator(Expression expression)
        {
            _resultStringBuilder = new StringBuilder();
            _expression = expression;
        }

        public string Translate()
        {
            Console.WriteLine($"Translating {_expression.ToString("C#")}");

            if (_resultStringBuilder.Length > 0)
            {
                throw new InvalidOperationException($"{nameof(FTSQueryExpressionTranslator)} was already used.");
            }
            
            Visit(_expression);

            return _resultStringBuilder.ToString();
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable) &&
                node.Method.Name == nameof(Queryable.Where))
            {
                var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }

            if (node.Method.DeclaringType == typeof(string))
            {
                VisitStringMethod(node);

                return node;
            }

            if (node.Method.DeclaringType == typeof(object))
            {
                VisitObjectMethod(node);

                return node;
            }
            
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    VisitEqualBinaryExpression(node);
                    break;

                default:
                    throw new NotSupportedException($"Expression: {node}. Operation {node.NodeType} is not supported");
            }

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultStringBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            _resultStringBuilder.Append(node.Value);

            return node;
        }
        
        private void VisitStringMethod(MethodCallExpression node)
        {
            if (node?.Object is MemberExpression member)
            {
                VisitMember(member);

                _resultStringBuilder.Append(OpenBracket);

                var argument = node.Arguments.First();
                switch (node.Method.Name)
                {
                    case nameof(string.Equals):
                        Visit(argument);
                        break;
                    case nameof(string.StartsWith):
                        Visit(argument);
                        _resultStringBuilder.Append(AsteriskWildcard);
                        break;
                    case nameof(string.EndsWith):
                        _resultStringBuilder.Append(AsteriskWildcard);
                        Visit(argument);
                        break;
                    case nameof(string.Contains):
                        _resultStringBuilder.Append(AsteriskWildcard);
                        Visit(argument);
                        _resultStringBuilder.Append(AsteriskWildcard);
                        break;
                    default:
                        throw new NotSupportedException(
                            $"Expression: \"{node}\". {node.Method.Name} string method is not supported yet.");
                }

                _resultStringBuilder.Append(CloseBracket);
            }
            else
            {
                throw new NotSupportedException($"Expression \"{node}\" is not supported yet.");
            }
        }

        private void VisitObjectMethod(MethodCallExpression node)
        {
            if (node?.Object is MemberExpression member)
            {
                VisitMember(member);

                _resultStringBuilder.Append(OpenBracket);

                switch (node.Method.Name)
                {
                    case nameof(object.Equals):
                        Visit(node.Arguments.First());
                        break;
                    default:
                        throw new NotSupportedException(
                            $"Expression: \"{node}\". {node.Method.Name} object method is not supported yet.");
                }

                _resultStringBuilder.Append(CloseBracket);
            }
            else
            {
                throw new NotSupportedException($"Expression \"{node}\" is not supported yet.");
            }
        }

        private void VisitEqualBinaryExpression(BinaryExpression node)
        {
            var memberAccessNode = node.Left as MemberExpression ?? node.Right as MemberExpression;
            var constantNode=node.Left as ConstantExpression ?? node.Right as ConstantExpression;
           
            if (memberAccessNode == null || constantNode == null)
            {
                throw new NotSupportedException(
                    $"Equal operation should contain member access and constant, but was \"{node}\"");
            }

            Visit(memberAccessNode);
            _resultStringBuilder.Append(OpenBracket);
            Visit(constantNode);
            _resultStringBuilder.Append(CloseBracket);
        }
    }
}