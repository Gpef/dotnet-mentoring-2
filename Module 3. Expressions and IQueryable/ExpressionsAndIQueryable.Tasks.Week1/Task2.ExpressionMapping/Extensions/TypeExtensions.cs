using System;
using System.Collections.Generic;
using System.Reflection;

namespace Task2.ExpressionMapping.Extensions
{
    public static class TypeExtensions
    { 
        public static List<MemberInfo> GetPropertiesAndFields(this Type type)
        {
            var members = new List<MemberInfo>();
            members.AddRange(type.GetProperties());
            members.AddRange(type.GetFields());

            return members;
        }
        
        public static ConstructorInfo VerifyAndGetConstructorWithoutParameters(this Type type)
        {
            var constructor = type.GetConstructor(new Type[] { });
            if (type == null)
            {
                throw new ArgumentException($"Default constructor for destination type was not found");
            }

            return constructor;
        }
        
    }
}