using System;
using System.Linq.Expressions;

namespace Task2.ExpressionMapping.Extensions
{
    public static class ExpressionExtensions
    {
        public static void ValidateIsMemberAccessExpression(this Expression expression, string paramName)
        {
            if (expression.NodeType != ExpressionType.MemberAccess)
            {
                throw new ArgumentException(
                    $"Must be member access (property or field) expression but was {expression.NodeType}", paramName);
            }
        }
    }
}