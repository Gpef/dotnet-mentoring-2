namespace Task2.ExpressionMapping
{
    public interface IMapper<TSource, TDestination>
    {
        TDestination Map(TSource source);
    }
}