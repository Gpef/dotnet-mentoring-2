using System.Reflection;

namespace Task2.ExpressionMapping
{
    public class MemberMapping
    {
        public MemberInfo Source { get; }
        public MemberInfo Destination { get; }

        public MemberMapping(MemberInfo source, MemberInfo destination)
        {
            Source = source;
            Destination = destination;
        }
    }
}