﻿using ExpressionToString;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Task2.ExpressionMapping.Extensions;

namespace Task2.ExpressionMapping
{
    public class MappingGenerator<TSource, TDestination>
    {
        private readonly Type _destType;
        private readonly Type _sourceType;
        private readonly ConstructorInfo _destConstructor;

        private readonly List<Expression> _mappingExpressions = new List<Expression>();

        private readonly List<MemberMapping> _memberToMap = new List<MemberMapping>();

        public MappingGenerator()
        {
            _sourceType = typeof(TSource);
            _destType = typeof(TDestination);

            _destConstructor = _destType.VerifyAndGetConstructorWithoutParameters();
        }

        /// <summary>
        /// Finalises mapping building and returns Mapper instance. 
        /// </summary>
        /// <returns>Mapper instance that can be used to map any <see cref="TDestination "/>type object</returns>
        public Mapper<TSource, TDestination> Build()
        {
            var sourceInstance = Expression.Parameter(_sourceType, "source");
            var destInstance = Expression.Variable(_destType, "dest");

            _mappingExpressions.Add(Expression.Assign(destInstance, Expression.New(_destConstructor)));


            foreach (var mapping in _memberToMap)
            {
                var sourceMember = Expression.PropertyOrField(sourceInstance, mapping.Source.Name);
                var destMember = Expression.PropertyOrField(destInstance, mapping.Destination.Name);

                _mappingExpressions.Add(Expression.Assign(destMember, sourceMember));
            }

            // Return destInstance
            _mappingExpressions.Add(destInstance);

            var body = Expression.Block(new[] { destInstance }, _mappingExpressions);

            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(body, sourceInstance);

            Console.WriteLine($"Mapping function: {mapFunction.ToString("C#")}");

            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }

        /// <summary>
        /// Generate mapping rules for fields and properties that are equally named in the
        /// source and destination types.
        /// </summary>
        /// <returns>Mapping generator to continue building mapping rules.</returns>
        public MappingGenerator<TSource, TDestination> ForEquallyNamed()
        {
            var sourceMembers = _sourceType.GetPropertiesAndFields();
            var destMembers = _destType.GetPropertiesAndFields();

            foreach (var sourceMember in sourceMembers)
            {
                var equallyNamedDestMember = destMembers.FirstOrDefault(pi => pi.Name == sourceMember.Name);

                if (null != equallyNamedDestMember &&
                    _memberToMap.All(mi => mi.Destination.Name != equallyNamedDestMember.Name))
                {
                    _memberToMap.Add(new MemberMapping(sourceMember, equallyNamedDestMember));
                }
            }

            return this;
        }

        /// <summary>
        /// Generate mapping rule of kind destinationMember value = sourceMember value.
        /// </summary>
        /// <param name="sourceMember">Source object property or field to get mapping value from.</param>
        /// <param name="destinationMember">Destination property or field to place mapping value into.</param>
        /// <typeparam name="TMemberType">Property or field type value.</typeparam>
        /// <returns>Mapping generator to continue building mapping rules.</returns>
        public MappingGenerator<TSource, TDestination> For<TMemberType>(
            Expression<Func<TSource, TMemberType>> sourceMember,
            Expression<Func<TDestination, TMemberType>> destinationMember)
        {
            sourceMember.Body.ValidateIsMemberAccessExpression(nameof(sourceMember));
            destinationMember.Body.ValidateIsMemberAccessExpression(nameof(sourceMember));

            var sourceMemberInfo = ((MemberExpression)sourceMember.Body).Member;
            var destMemberInfo = ((MemberExpression)destinationMember.Body).Member;

            var existingMapping = _memberToMap.FirstOrDefault(mm => mm.Destination.Name == destMemberInfo.Name);
            if (null != existingMapping)
            {
                _memberToMap.Remove(existingMapping);
            }

            _memberToMap.Add(new MemberMapping(sourceMemberInfo, destMemberInfo));

            return this;
        }
    }
}