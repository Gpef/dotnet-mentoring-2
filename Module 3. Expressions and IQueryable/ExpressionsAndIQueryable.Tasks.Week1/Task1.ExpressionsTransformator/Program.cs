﻿/*
 * Create a class based on ExpressionVisitor, which makes expression tree transformation:
 * 1. converts expressions like <variable> + 1 to increment operations, <variable> - 1 - into decrement operations.
 * 2. changes parameter values in a lambda expression to constants, taking the following as transformation parameters:
 *    - source expression;
 *    - dictionary: <parameter name: value for replacement>
 * The results could be printed in console or checked via Debugger using any Visualizer.
 */

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Task1.ExpressionsTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Expression Visitor for increment/decrement.");
            Console.WriteLine();

            Expression<Func<int, int>> increment = x => 1 + x;
            Expression<Func<int, int>> decrement = x => x - 1;
            Expression<Func<int, int>> complex = x => x + x + 1 - (x - 1);

            var expressions = new List<Expression<Func<int, int>>> { increment, decrement, complex };

            foreach (var expression in expressions)
            {
                var visitor = new IncDecExpressionVisitor(expression);

                Console.WriteLine($"Expression: {expression}");
                Console.WriteLine($"Visited expression: {visitor.Visit()}");
            }

            Expression<Func<int, int, int, int>> veryComplex = (x, y, z) => x + 1 + (x - 1) + y + z + x + 2;
            var parameters = new Dictionary<string, object> { { "x", 3 }, { "y", 9 }, { "z", 0 } };
            var replaceVisitor = new IncDecExpressionVisitor(veryComplex, parameters);

            var visitedVeryComplex = replaceVisitor.VisitAndConvertParameters();
            Console.WriteLine($"Very complex: {veryComplex}");
            Console.WriteLine($"Visited very complex: {visitedVeryComplex}");

            Console.ReadLine();
        }
    }
}