﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Task1.ExpressionsTransformer
{
    public class IncDecExpressionVisitor : ExpressionVisitor
    {
        private readonly Expression _expressionToConvert;

        private IEnumerable<string> _parametersToReplace = new List<string>();
        private Dictionary<string, object> _parametersToReplaceWith = new Dictionary<string, object>();

        public IncDecExpressionVisitor(Expression expression) : this(expression, new Dictionary<string, object>())
        {
        }

        public IncDecExpressionVisitor(Expression expression, Dictionary<string, object> parametersToReplace)
        {
            _expressionToConvert = expression;
            _parametersToReplaceWith = parametersToReplace;
        }

        public Expression VisitAndConvertParameters()
        {
            if (_expressionToConvert.NodeType == ExpressionType.Lambda)
            {
                var lambda = (LambdaExpression)_expressionToConvert;

                _parametersToReplace = lambda.Parameters.Where(p => _parametersToReplaceWith.Keys.Contains(p.Name)).Select(p => p.Name);
                var visitedBody = Visit(lambda.Body);

                return visitedBody;
            }

            return _expressionToConvert;
        }

        public Expression Visit() => Visit(_expressionToConvert);

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (_parametersToReplace.Contains(node.Name))
            {
                return Expression.Constant(_parametersToReplaceWith[node.Name]);
            }

            return base.VisitParameter(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Add:
                    return ReplaceAdd(node);
                case ExpressionType.Subtract:
                    return ReplaceSubtract(node);
                default:
                    return base.VisitBinary(node);
            }
        }

        private Expression ReplaceAdd(BinaryExpression node) =>
            IsOperationWithParameterAndConstantOne(node, out var parameter)
                ? Expression.Increment(parameter)
                : base.VisitBinary(node);

        private Expression ReplaceSubtract(BinaryExpression node) =>
            IsOperationWithParameterAndConstantOne(node, out var parameter)
                ? Expression.Decrement(parameter)
                : base.VisitBinary(node);

        private static bool IsOperationWithParameterAndConstantOne(BinaryExpression node, out ParameterExpression parameter)
        {
            parameter = node.Left as ParameterExpression ?? node.Right as ParameterExpression;
            var constant = node.Left as ConstantExpression ?? node.Right as ConstantExpression;

            return parameter != null && constant != null &&
                   constant.Type == typeof(int) &&
                   (int)constant.Value == 1;
        }
    }
}
