﻿namespace Task2.ExpressionMapping.Tests.Models
{
    public class Bar
    {
       public string StringProperty { get; set; }
       public int IntProperty { get; set; }
       public bool? BoolMember;
       public int IntPropertyTwo { get; set; }
       
       public object ObjectField { get; set; }
    }
}
