﻿namespace Task2.ExpressionMapping.Tests.Models
{
    public class Foo
    {
        public string StringProperty { get; set; }
        public int IntProperty { get; set; }
        public bool? BoolMember { get; set; }
        public int IntPropertySecond { get; set; }
        
        public object ObjectProperty { get; set; }
    }
}