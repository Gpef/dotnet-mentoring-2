using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NUnit.Framework;
using Task2.ExpressionMapping.Tests.Models;

namespace Task2.ExpressionMapping.Tests
{
    [TestFixture]
    public class ExpressionMappingNegativeTests : ExpressionMappingTestBase
    {
        [Test]
        [TestCaseSource(nameof(NotPropertyOrFieldMemberExpressions))]
        public void Map_TryToCreateRule_ForInvalidExpression(
            Expression<Func<Foo, object>> sourceExpression,
            Expression<Func<Bar, object>> destExpression)
        {
            const string expectedMessageStart = "Must be member access (property or field) expression";

            var mapper = new MappingGenerator<Foo, Bar>();
            var exception =
                Assert.Throws<ArgumentException>(() => mapper = mapper.For(sourceExpression, destExpression));
            StringAssert.StartsWith(expectedMessageStart, exception.Message);
        }

        [Test]
        public void Map_MapManuallyObjectMemberWithBoxedObjectInIt()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .For(foo => foo.ObjectProperty, bar => bar.ObjectField).Build();

            var fooToMap = GenerateFoo();
            fooToMap.ObjectProperty = new string("just a string");
            var result = mapper.Map(fooToMap);
            Assert.AreEqual(fooToMap.ObjectProperty, result.ObjectField);
        }

        private static IEnumerable<TestCaseData> NotPropertyOrFieldMemberExpressions()
        {
            yield return CreateExpressionsData(foo => foo.ToString(), bar => bar.StringProperty);
            yield return CreateExpressionsData(foo => "some string", bar => bar.StringProperty);
            yield return CreateExpressionsData(foo => new Foo(), bar => bar.StringProperty);
            yield return CreateExpressionsData(foo => (object) foo.BoolMember, bar => bar.StringProperty);

            yield return CreateExpressionsData(foo => foo.StringProperty, bar => bar.ToString());
            yield return CreateExpressionsData(foo => foo.StringProperty, bar => "some string");
            yield return CreateExpressionsData(foo => foo.StringProperty, bar => new Bar());
            yield return CreateExpressionsData(foo => foo.StringProperty, bar => (object) bar.BoolMember);
        }

        private static TestCaseData CreateExpressionsData(
            Expression<Func<Foo, object>> sourceExpression,
            Expression<Func<Bar, object>> destExpression) => new TestCaseData(sourceExpression, destExpression);
    }
}