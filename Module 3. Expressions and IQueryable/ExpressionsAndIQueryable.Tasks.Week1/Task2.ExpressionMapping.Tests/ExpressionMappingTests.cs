using NUnit.Framework;
using Task2.ExpressionMapping.Tests.Models;

namespace Task2.ExpressionMapping.Tests
{
    [TestFixture]
    public class ExpressionMappingTests : ExpressionMappingTestBase
    {
        [Test]
        public void MapEquallyNamedFieldsOnly()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .ForEquallyNamed()
                .Build();

            var fooToMap = GenerateFoo();
            var result = mapper.Map(fooToMap);
            AssertOnlyEquallyNamedFieldsAreEqual(fooToMap, result);
        }

        [Test]
        public void MapAllFields_Manually()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .For(foo => foo.IntProperty, bar => bar.IntProperty)
                .For(foo => foo.BoolMember, bar => bar.BoolMember)
                .For(foo => foo.StringProperty, bar => bar.StringProperty)
                .For(foo => foo.IntPropertySecond, bar => bar.IntPropertyTwo)
                .Build();

            var fooToMap = GenerateFoo();
            var result = mapper.Map(fooToMap);
            AssertAllFieldsAreEqual(fooToMap, result);
        }

        [Test]
        public void MapAllFields_EquallyNamedFirst_ThenManually()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .ForEquallyNamed()
                .For(foo => foo.IntPropertySecond, bar => bar.IntPropertyTwo)
                .Build();

            var fooToMap = GenerateFoo();
            var result = mapper.Map(fooToMap);
            AssertAllFieldsAreEqual(fooToMap, result);
        }

        [Test]
        public void Map_EquallyNamedFirst_ThenRemapManually()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .ForEquallyNamed()
                .For(foo => foo.IntPropertySecond, bar => bar.IntProperty)
                .Build();

            var fooToMap = GenerateFoo();
            var result = mapper.Map(fooToMap);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(fooToMap.IntPropertySecond, result.IntProperty);
                Assert.AreEqual(fooToMap.BoolMember, result.BoolMember);
                Assert.AreEqual(fooToMap.StringProperty, result.StringProperty);
                Assert.AreEqual(default(int), result.IntPropertyTwo);
            });
        }

        [Test]
        public void Map_Manually_NotEquallyNamedFields()
        {
            var mapper = new MappingGenerator<Foo, Bar>()
                .For(foo => foo.IntProperty, bar => bar.IntPropertyTwo)
                .For(foo => foo.IntPropertySecond, bar => bar.IntProperty)
                .Build();

            var fooToMap = GenerateFoo();
            var result = mapper.Map(fooToMap);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(fooToMap.IntProperty, result.IntPropertyTwo);
                Assert.AreEqual(fooToMap.IntPropertySecond, result.IntProperty);

                Assert.AreEqual(default(bool?), result.BoolMember);
                Assert.AreEqual(default(string), result.StringProperty);
            });
        }
    }
}