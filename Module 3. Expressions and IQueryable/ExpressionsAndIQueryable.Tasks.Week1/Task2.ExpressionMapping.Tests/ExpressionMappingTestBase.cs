using Bogus;
using NUnit.Framework;
using Task2.ExpressionMapping.Tests.Models;

namespace Task2.ExpressionMapping.Tests
{
    [TestFixture]
    public abstract class ExpressionMappingTestBase
    {
        protected Foo GenerateFoo()
        {
            var faker = new Faker<Foo>().Rules((f, o) =>
            {
                o.BoolMember = f.Random.Bool();
                o.IntProperty = f.Random.Int();
                o.StringProperty = f.Random.AlphaNumeric(10);
                o.IntPropertySecond = f.Random.Int();
            });

            return faker.Generate();
        }

        protected static void AssertOnlyEquallyNamedFieldsAreEqual(Foo source, Bar destination)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(source.BoolMember, destination.BoolMember);
                Assert.AreEqual(source.IntProperty, destination.IntProperty);
                Assert.AreEqual(source.StringProperty, destination.StringProperty);
                Assert.AreNotEqual(source.IntPropertySecond, destination.IntPropertyTwo);
            });
        }

        protected static void AssertAllFieldsAreEqual(Foo source, Bar destination)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(source.BoolMember, destination.BoolMember);
                Assert.AreEqual(source.IntProperty, destination.IntProperty);
                Assert.AreEqual(source.StringProperty, destination.StringProperty);
                Assert.AreEqual(source.IntPropertySecond, destination.IntPropertyTwo);
            });
        }
    }
}
